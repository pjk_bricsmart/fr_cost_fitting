---
title: "FRACTool"
date: "`r Sys.Date()`"
output:
  rmdformats::readthedown:
    highlight: kate
---


```{r setup, echo=FALSE, cache=FALSE}
library(knitr)
library(rmdformats)

## Global options
options(max.print="75")
opts_chunk$set(echo=FALSE,
	             cache=TRUE,
               prompt=FALSE,
               tidy=TRUE,
               comment=NA,
               message=FALSE,
               warning=FALSE)
opts_knit$set(width=75)
```

# Data  
The data for this work, provided by Henry WIGGINS, was delivered in an Excel file: **20200214 FR Cost Comparison Template (Snyder Data).xlsx**. The experimentally measured data is collected in the **All Data** worksheet. The salient data in this worksheet includes

* **FR** (**F**riction **R**educer): Products include  
    * DR-22430  
    * DR-42430  
    * FR-HPA  
    * FR-HSU  
    * HiFlo 5  
    * Kemflow A4251  
    * Plexslick 920  
    * Plexslick 922  
    * Plexslick 930  
    * Plexslick 953  
    * Plexslick 959  
    * Plexslick 986C Boost  
    * Plexslick V999  
    * Plexslick V993  
    * Plexslick V997  
* **Loading (gpt)**: 0.1, 0.2, 0.3, 0.5, 1  
* **Water**: Fresh, 50k, 100k, 150k, 200k  

# Example Subset
