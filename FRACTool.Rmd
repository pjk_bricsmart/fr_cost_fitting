---
title: "Title"
author: "[John Zobolas](https://github.com/bblodfon)"
date: "Last updated: `r format(Sys.time(), '%d %B, %Y')`"
output:
  html_document:
    css: style.css
    theme: united
    toc: true
    toc_float:
      collapsed: false
      smooth_scroll: true
    toc_depth: 3
    number_sections: false
    code_folding: hide
    code_download: true
---


```{r setup, echo=FALSE, cache=FALSE}
library(knitr)
library(rmdformats)

## Global options
options(max.print="75")
opts_chunk$set(echo=FALSE,
	             cache=TRUE,
               prompt=FALSE,
               tidy=TRUE,
               comment=NA,
               message=FALSE,
               warning=FALSE)
opts_knit$set(width=75)

library(kableExtra)
```

# A

# Data  
The data for this work, provided by Henry WIGGINS, was delivered in an Excel file: **20200214 FR Cost Comparison Template (Snyder Data).xlsx**. The experimentally measured data is collected in the **All Data** worksheet. The salient data in this worksheet includes

* **FR** (**F**riction **R**educer): Products include  
    * DR-22430  
    * DR-42430  
    * FR-HPA  
    * FR-HSU  
    * HiFlo 5  
    * Kemflow A4251  
    * Plexslick 920  
    * Plexslick 922  
    * Plexslick 930  
    * Plexslick 953  
    * Plexslick 959  
    * Plexslick 986C Boost  
    * Plexslick V999  
    * Plexslick V993  
    * Plexslick V997  
* **Loading (gpt)**: 0.1, 0.2, 0.3, 0.5, 1  
* **Water**: Fresh, 50k, 100k, 150k, 200k  

# Example Subset

```{r data-preparation, cache = TRUE, echo = FALSE}
library(magrittr)
library(plyr)
library(dplyr)
library(stats)
library(tidyr)
library(ggplot2)
library(ggthemes)
library(htmlwidgets)
library(plotly)
library(officer)

library(SciViews)
library(drc)
library(nlme)

# library(aomisc)

df <- readxl::read_xlsx('20200214 FR Cost Comparison Template (Snyder Data).xlsx', sheet = 'All Data')

column_names <- c('Expt', 'FR', 'FR Type', 'LOOP', 'Supplier', 'Loading (gpt)', 'Water',
                  '0.00', '0.17', '0.33', '0.50', '0.67', '0.83', '1.00', '1.17', '1.33',
                  '1.50', '1.67', '1.83', '2.00', '2.17', '2.33', '2.50', '2.67', '2.83',
                  '3.00', '3.17', '3.33', '3.50', '3.67', '3.83', '4.00', '4.17', '4.33',
                  '4.50', '4.67', '4.83', '5.00', 'Average', '3 gpt', '6 gpt', 'Date',
                  'Batch No', 'Sample', 'Pipe ID', 'Flow Rate')

colnames(df) <- column_names

df <- df %>%
  dplyr::select(-'3 gpt', -'6 gpt', -Date, -'Batch No', -Sample, -'Pipe ID', -'Flow Rate') %>%
  filter(`Loading (gpt)` != 'A') %>%
  filter(`Loading (gpt)` != 'k')

# alles <- complete.cases(df)

# df <- df[alles, ]

qaz <- data.frame(rowMeans(df[ , 11:38]))
colnames(qaz) <- c('Avg30sec')
df <- cbind(df, qaz)
qaz <- data.frame(rowMeans(df[ , 14:38]))
colnames(qaz) <- c('Avg1min')
df <- cbind(df, qaz)

df <- df %>%
  dplyr::select(-Average)
```

```{r salient-data, cache = TRUE, echo = FALSE}
df2 <- df %>%
  filter(FR == 'FR-HPA' | FR == 'DR-22430') %>%
  filter(Water == '50k')
df3 <- df %>%
  filter(FR == 'Plexslick 953' | FR == 'Plexslick 959') %>%
  filter(Water == 'Fresh')
df4 <- rbind(df2, df3) %>%
  dplyr::select(FR, 'Loading (gpt)', Water, `0.50`, `1.00`, `2.00`, `5.00`, Avg1min)

knitr::kable(df4, digits = 3, caption = 'A subset of available data.') %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed", "responsive", full_width = T, position = "center", font_size = 6)) %>%
  scroll_box(height = "400px")
```

# C

