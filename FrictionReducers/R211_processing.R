# instantiate environment
library(tidyverse)
library(readxl)
library(magrittr)
library(usethis)
library(lubridate)
library(janitor)
library(naniar)

# R211

# Nov 01 2018 to Nov 15 2018
R211_nov0118 <- readxl::read_xls("BigDataDump/R211 11-1-18_ 11-15-18.xls",
                                 skip = 14) %>%
  mutate(hr = hour(Time)) %>%
  mutate(minu = minute(Time)) %>%
  mutate(sec = second(Time)) %>%
  mutate(dateTime = paste0(Date, ' ', hr, ':', minu, ':', sec)) %>%
  mutate(dateTime = ymd_hms(dateTime)) %>%
  select(-hr, -minu, -sec) %>%
  clean_names() %>%
  data.frame()
# vis_miss(R211_nov0118)

# Nov 16 2018 to Nov 30 2018
R211_nov1618 <- readxl::read_xls("BigDataDump/R211 11-16-18_ 11-30-18.xls",
                                 skip = 14) %>%
  mutate(hr = hour(Time)) %>%
  mutate(minu = minute(Time)) %>%
  mutate(sec = second(Time)) %>%
  mutate(dateTime = paste0(Date, ' ', hr, ':', minu, ':', sec)) %>%
  mutate(dateTime = ymd_hms(dateTime)) %>%
  select(-hr, -minu, -sec) %>%
  clean_names() %>%
  data.frame()
# vis_miss(R211_nov1618)

# Dec 01 2018 to Dec 15 2018
R211_dec0118 <- readxl::read_xls("BigDataDump/R211 12-1-18_ 12-15-18.xls",
                                 skip = 14) %>%
  mutate(hr = hour(Time)) %>%
  mutate(minu = minute(Time)) %>%
  mutate(sec = second(Time)) %>%
  mutate(dateTime = paste0(Date, ' ', hr, ':', minu, ':', sec)) %>%
  mutate(dateTime = ymd_hms(dateTime)) %>%
  select(-hr, -minu, -sec) %>%
  clean_names() %>%
  data.frame()

# Dec 16 2018 to Dec 31 2018
R211_dec1618 <- readxl::read_xls("BigDataDump/R211 12-16-18_ 12-31-18.xls",
                                 skip = 14) %>%
  mutate(hr = hour(Time)) %>%
  mutate(minu = minute(Time)) %>%
  mutate(sec = second(Time)) %>%
  mutate(dateTime = paste0(Date, ' ', hr, ':', minu, ':', sec)) %>%
  mutate(dateTime = ymd_hms(dateTime)) %>%
  select(-hr, -minu, -sec) %>%
  clean_names() %>%
  data.frame()

# Jan 01 2019 to Jan 15 2019
R211_jan0119 <- readxl::read_xls("BigDataDump/New Data Dump/batches-4-4/R211/R211-1-1-19_1-15-19.xls",
                                 skip = 14) %>%
  mutate(hr = hour(Time)) %>%
  mutate(minu = minute(Time)) %>%
  mutate(sec = second(Time)) %>%
  mutate(dateTime = paste0(Date, ' ', hr, ':', minu, ':', sec)) %>%
  mutate(dateTime = ymd_hms(dateTime)) %>%
  select(-hr, -minu, -sec) %>%
  clean_names() %>%
  data.frame()

# Jan 17 2019 to Jan 31 2019
R211_jan1619 <- readxl::read_xls("BigDataDump/New Data Dump/batches-4-4/R211/R211-1-16-19_1-31-19.xls",
                                 skip = 13) %>%
  mutate(hr = hour(Time)) %>%
  mutate(minu = minute(Time)) %>%
  mutate(sec = second(Time)) %>%
  mutate(dateTime = paste0(Date, ' ', hr, ':', minu, ':', sec)) %>%
  mutate(dateTime = ymd_hms(dateTime)) %>%
  select(-hr, -minu, -sec) %>%
  clean_names() %>%
  data.frame()

# Feb 01 2019 to Feb 15 2019
R211_feb0119 <- readxl::read_xls("BigDataDump/New Data Dump/batches-4-4/R211/R211-2-1-19_2-15-19.xls",
                                 skip = 13) %>%
  mutate(hr = hour(Time)) %>%
  mutate(minu = minute(Time)) %>%
  mutate(sec = second(Time)) %>%
  mutate(dateTime = paste0(Date, ' ', hr, ':', minu, ':', sec)) %>%
  mutate(dateTime = ymd_hms(dateTime)) %>%
  select(-hr, -minu, -sec) %>%
  clean_names() %>%
  data.frame()

# Feb 16 2019 to Feb 28 2019
R211_feb1619 <- readxl::read_xls("BigDataDump/New Data Dump/batches-4-4/R211/R211-2-16-19_2-28-19.xls",
                                 skip = 13) %>%
  mutate(hr = hour(Time)) %>%
  mutate(minu = minute(Time)) %>%
  mutate(sec = second(Time)) %>%
  mutate(dateTime = paste0(Date, ' ', hr, ':', minu, ':', sec)) %>%
  mutate(dateTime = ymd_hms(dateTime)) %>%
  select(-hr, -minu, -sec) %>%
  clean_names() %>%
  data.frame()

# Mar 01 2019 to Mar 15 2019
R211_mar0119 <- readxl::read_xls("BigDataDump/New Data Dump/batches-4-4/R211/R211-3-1-19_3-15-19.xls",
                                 skip = 13) %>%
  mutate(hr = hour(Time)) %>%
  mutate(minu = minute(Time)) %>%
  mutate(sec = second(Time)) %>%
  mutate(dateTime = paste0(Date, ' ', hr, ':', minu, ':', sec)) %>%
  mutate(dateTime = ymd_hms(dateTime)) %>%
  select(-hr, -minu, -sec) %>%
  clean_names() %>%
  data.frame()

# Mar 16 2019 to Mar 31 2019
R211_mar1619 <- readxl::read_xls("BigDataDump/New Data Dump/batches-4-4/R211/R211-3-16-19_3-31-19.xls",
                                 skip = 13) %>%
  mutate(hr = hour(Time)) %>%
  mutate(minu = minute(Time)) %>%
  mutate(sec = second(Time)) %>%
  mutate(dateTime = paste0(Date, ' ', hr, ':', minu, ':', sec)) %>%
  mutate(dateTime = ymd_hms(dateTime)) %>%
  select(-hr, -minu, -sec) %>%
  clean_names() %>%
  data.frame()

# Apr 01 2019 to Apr 15 2019
R211_apr0119 <- readxl::read_xls("BigDataDump/New Data Dump/batches-4-4/R211/R211_4-1-19_4-15-19.xls",
                                 skip = 13) %>%
  mutate(hr = hour(Time)) %>%
  mutate(minu = minute(Time)) %>%
  mutate(sec = second(Time)) %>%
  mutate(dateTime = paste0(Date, ' ', hr, ':', minu, ':', sec)) %>%
  mutate(dateTime = ymd_hms(dateTime)) %>%
  select(-hr, -minu, -sec) %>%
  clean_names() %>%
  data.frame()

# Apr 16 2019 to Apr 30 2019
R211_apr1619 <- readxl::read_xls("BigDataDump/New Data Dump/batches-4-4/R211/R211_4-16-19_4-30-19.xls",
                                 skip = 13) %>%
  mutate(hr = hour(Time)) %>%
  mutate(minu = minute(Time)) %>%
  mutate(sec = second(Time)) %>%
  mutate(dateTime = paste0(Date, ' ', hr, ':', minu, ':', sec)) %>%
  mutate(dateTime = ymd_hms(dateTime)) %>%
  select(-hr, -minu, -sec) %>%
  clean_names() %>%
  data.frame()

# May 01 2019 to May 15 2019
R211_may0119 <- readxl::read_xls("BigDataDump/New Data Dump/batches-4-4/R211/R211_5-1-19_5-15-19.xls",
                                 skip = 13) %>%
  mutate(hr = hour(Time)) %>%
  mutate(minu = minute(Time)) %>%
  mutate(sec = second(Time)) %>%
  mutate(dateTime = paste0(Date, ' ', hr, ':', minu, ':', sec)) %>%
  mutate(dateTime = ymd_hms(dateTime)) %>%
  select(-hr, -minu, -sec) %>%
  clean_names() %>%
  data.frame()

# May 16 2019 to May 30 2019
R211_may1619 <- readxl::read_xls("BigDataDump/New Data Dump/batches-4-4/R211/R211_5-16-19_5-30-19.xls",
                                 skip = 13) %>%
  mutate(hr = hour(Time)) %>%
  mutate(minu = minute(Time)) %>%
  mutate(sec = second(Time)) %>%
  mutate(dateTime = paste0(Date, ' ', hr, ':', minu, ':', sec)) %>%
  mutate(dateTime = ymd_hms(dateTime)) %>%
  select(-hr, -minu, -sec) %>%
  clean_names() %>%
  data.frame()

# May 31 2019 to Jun 06 2019
R211_jun0119 <- readxl::read_xls("BigDataDump/New Data Dump/batches-4-4/R211/R211_5-31-19_6-6-19 1145AM.xls",
                                 skip = 13) %>%
  mutate(hr = hour(Time)) %>%
  mutate(minu = minute(Time)) %>%
  mutate(sec = second(Time)) %>%
  mutate(dateTime = paste0(Date, ' ', hr, ':', minu, ':', sec)) %>%
  mutate(dateTime = ymd_hms(dateTime)) %>%
  select(-hr, -minu, -sec) %>%
  clean_names() %>%
  data.frame()

R211 <- rbind(R211_nov0118, R211_nov1618,
              R211_dec0118, R211_dec1618,
              R211_jan0119, R211_jan1619,
              R211_feb0119, R211_feb1619,
              R211_mar0119, R211_mar1619,
              R211_apr0119, R211_apr1619,
              R211_may0119, R211_may1619,
              R211_jun0119)

saveRDS(R211, 'R211.RData')

sum(is.na(R211$msecs))
sum(is.na(R211$msecs))/dim(R211)[1]
sum(is.na(R211$t5012_211pres))
sum(is.na(R211$t5012_211pres))/dim(R211)[1]
sum(is.na(R211$t5012_211temp))
sum(is.na(R211$t5012_211temp))/dim(R211)[1]
sum(is.na(R211$t5012_211total))
sum(is.na(R211$t5012_211total))/dim(R211)[1]
sum(is.na(R211$t301_flow211))
sum(is.na(R211$t301_flow211))/dim(R211)[1]
sum(is.na(R211$t5012_tcoolin))
sum(is.na(R211$t5012_tcoolin))/dim(R211)[1]
sum(is.na(R211$t5012_tcoolout))
sum(is.na(R211$t5012_tcoolout))/dim(R211)[1]
sum(is.na(R211$t5012_211flow))
sum(is.na(R211$t5012_211flow))/dim(R211)[1]
