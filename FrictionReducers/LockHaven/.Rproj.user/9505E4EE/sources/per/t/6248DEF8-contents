suppressMessages(library(tint))
suppressMessages(library(tidyverse))
suppressMessages(library(readxl))
suppressMessages(library(magrittr))
suppressMessages(library(usethis))
suppressMessages(library(lubridate))
suppressMessages(library(janitor))
suppressMessages(library(ggplot2))
suppressMessages(library(ggthemes))
suppressMessages(library(gridExtra))
suppressMessages(library(here))
suppressMessages(library(vtree))
suppressMessages(library(janitor))
suppressMessages(library(naniar))
suppressMessages(library(kableExtra))
suppressMessages(library(stringr))

df <-
  read.csv(here('data', 'FR_MC_Batch_Steps_2019_10_01.csv')) %>% 
  clean_names()  %>%
  mutate(starttime = as_datetime(starttime)) %>%
  mutate(endtime = as_datetime(endtime)) %>%
  arrange(starttime) %>%
  data.frame()

# remove rows with ALL entries == NA
rowNA <- apply(df, 1, function(df) all(is.na(df)))
df <- df[ !rowNA, ]

df$row <- seq.int(nrow(df))
df$label <- stringr::str_c(df$procedure, df$row)

dfb <-
  df %>%
  filter(batchid == '13007') %>%
  select(label, starttime, endtime, uniqueid, batchid) %>%
  mutate(label = factor(label, levels = rev(unique(label)), ordered  = TRUE)) %>%
  data.frame()
ggplot(dfb,
       aes(
         x = starttime,
         xend = endtime,
         y = label,
         yend = label,
         color = batchid
       )) +
  geom_segment(size = 2) +
ggtitle(paste0('13007', '\n', 'start = ', min(dfb$starttime), '\n', 'end = ', max(dfb$endtime)))

dfb <-
  df %>%
  filter(batchid == '14006') %>%
  select(label, starttime, endtime, uniqueid, batchid) %>%
  mutate(label = factor(label, levels = rev(unique(label)), ordered  = TRUE)) %>%
  data.frame()
ggplot(dfb,
       aes(
         x = starttime,
         xend = endtime,
         y = label,
         yend = label,
         color = batchid
       )) +
  geom_segment(size = 2) +
  ggtitle(paste0('14006', '\n', 'start = ', min(dfb$starttime), '\n', 'end = ', max(dfb$endtime)))

dfb <-
  df %>%
  filter(batchid == '20004') %>%
  select(label, starttime, endtime, uniqueid, batchid) %>%
  mutate(label = factor(label, levels = rev(unique(label)), ordered  = TRUE)) %>%
  data.frame()
ggplot(dfb,
       aes(
         x = starttime,
         xend = endtime,
         y = label,
         yend = label,
         color = batchid
       )) +
  geom_segment(size = 2) +
  ggtitle(paste0('20004', '\n', 'start = ', min(dfb$starttime), '\n', 'end = ', max(dfb$endtime)))

dfb <-
  df %>%
  filter(batchid == '24004') %>%
  select(label, starttime, endtime, uniqueid, batchid) %>%
  mutate(label = factor(label, levels = rev(unique(label)), ordered  = TRUE)) %>%
  data.frame()
ggplot(dfb,
       aes(
         x = starttime,
         xend = endtime,
         y = label,
         yend = label,
         color = batchid
       )) +
  geom_segment(size = 2) +
  ggtitle(paste0('24004', '\n', 'start = ', min(dfb$starttime), '\n', 'end = ', max(dfb$endtime)))

dfb <-
  df %>%
  filter(batchid == '24006') %>%
  select(label, starttime, endtime, uniqueid, batchid) %>%
  mutate(label = factor(label, levels = rev(unique(label)), ordered  = TRUE)) %>%
  data.frame()
ggplot(dfb,
       aes(
         x = starttime,
         xend = endtime,
         y = label,
         yend = label,
         color = batchid
       )) +
  geom_segment(size = 2) +
  ggtitle(paste0('24006', '\n', 'start = ', min(dfb$starttime), '\n', 'end = ', max(dfb$endtime)))

dfb <-
  df %>%
  filter(batchid == '27007') %>%
  select(label, starttime, endtime, uniqueid, batchid) %>%
  mutate(label = factor(label, levels = rev(unique(label)), ordered  = TRUE)) %>%
  data.frame()
ggplot(dfb,
       aes(
         x = starttime,
         xend = endtime,
         y = label,
         yend = label,
         color = batchid
       )) +
  geom_segment(size = 2) +
  ggtitle(paste0('27007', '\n', 'start = ', min(dfb$starttime), '\n', 'end = ', max(dfb$endtime)))

dfb <-
  df %>%
  filter(batchid == '28001') %>%
  select(label, starttime, endtime, uniqueid, batchid) %>%
  mutate(label = factor(label, levels = rev(unique(label)), ordered  = TRUE)) %>%
  data.frame()
ggplot(dfb,
       aes(
         x = starttime,
         xend = endtime,
         y = label,
         yend = label,
         color = batchid
       )) +
  geom_segment(size = 2) +
  ggtitle(paste0('28001', '\n', 'start = ', min(dfb$starttime), '\n', 'end = ', max(dfb$endtime)))

dfb <-
  df %>%
  filter(batchid == '28005') %>%
  select(label, starttime, endtime, uniqueid, batchid) %>%
  mutate(label = factor(label, levels = rev(unique(label)), ordered  = TRUE)) %>%
  data.frame()
ggplot(dfb,
       aes(
         x = starttime,
         xend = endtime,
         y = label,
         yend = label,
         color = batchid
       )) +
  geom_segment(size = 2) +
  ggtitle(paste0('28005', '\n', 'start = ', min(dfb$starttime), '\n', 'end = ', max(dfb$endtime)))

dfb <-
  df %>%
  filter(batchid == '28007') %>%
  select(label, starttime, endtime, uniqueid, batchid) %>%
  mutate(label = factor(label, levels = rev(unique(label)), ordered  = TRUE)) %>%
  data.frame()
ggplot(dfb,
       aes(
         x = starttime,
         xend = endtime,
         y = label,
         yend = label,
         color = batchid
       )) +
  geom_segment(size = 2) +
  ggtitle(paste0('28007', '\n', 'start = ', min(dfb$starttime), '\n', 'end = ', max(dfb$endtime)))

dfb <-
  df %>%
  filter(batchid == 'CT19H10001') %>%
  select(label, starttime, endtime, uniqueid, batchid) %>%
  mutate(label = factor(label, levels = rev(unique(label)), ordered  = TRUE)) %>%
  data.frame()
ggplot(dfb,
       aes(
         x = starttime,
         xend = endtime,
         y = label,
         yend = label,
         color = batchid
       )) +
  geom_segment(size = 2) +
  ggtitle(paste0('CT19H10001', '\n', 'start = ', min(dfb$starttime), '\n', 'end = ', max(dfb$endtime)))

dfb <-
  df %>%
  filter(batchid == 'CT19H12001') %>%
  select(label, starttime, endtime, uniqueid, batchid) %>%
  mutate(label = factor(label, levels = rev(unique(label)), ordered  = TRUE)) %>%
  data.frame()
ggplot(dfb,
       aes(
         x = starttime,
         xend = endtime,
         y = label,
         yend = label,
         color = batchid
       )) +
  geom_segment(size = 2) +
  ggtitle(paste0('CT19H12001', '\n', 'start = ', min(dfb$starttime), '\n', 'end = ', max(dfb$endtime)))

dfb <-
  df %>%
  filter(batchid == 'CT19H12003') %>%
  select(label, starttime, endtime, uniqueid, batchid) %>%
  mutate(label = factor(label, levels = rev(unique(label)), ordered  = TRUE)) %>%
  data.frame()
ggplot(dfb,
       aes(
         x = starttime,
         xend = endtime,
         y = label,
         yend = label,
         color = batchid
       )) +
  geom_segment(size = 2) +
  ggtitle(paste0('CT19H12003', '\n', 'start = ', min(dfb$starttime), '\n', 'end = ', max(dfb$endtime)))

dfb <-
  df %>%
  filter(batchid == 'CT19H14008') %>%
  select(label, starttime, endtime, uniqueid, batchid) %>%
  mutate(label = factor(label, levels = rev(unique(label)), ordered  = TRUE)) %>%
  data.frame()
ggplot(dfb,
       aes(
         x = starttime,
         xend = endtime,
         y = label,
         yend = label,
         color = batchid
       )) +
  geom_segment(size = 2) +
  ggtitle(paste0('CT19H14008', '\n', 'start = ', min(dfb$starttime), '\n', 'end = ', max(dfb$endtime)))

dfb <-
  df %>%
  filter(batchid == 'CT19H29005') %>%
  select(label, starttime, endtime, uniqueid, batchid) %>%
  mutate(label = factor(label, levels = rev(unique(label)), ordered  = TRUE)) %>%
  data.frame()
ggplot(dfb,
       aes(
         x = starttime,
         xend = endtime,
         y = label,
         yend = label,
         color = batchid
       )) +
  geom_segment(size = 2) +
  ggtitle(paste0('CT19H29005', '\n', 'start = ', min(dfb$starttime), '\n', 'end = ', max(dfb$endtime)))

dfb <-
  df %>%
  filter(batchid == 'CT19H29007') %>%
  select(label, starttime, endtime, uniqueid, batchid) %>%
  mutate(label = factor(label, levels = rev(unique(label)), ordered  = TRUE)) %>%
  data.frame()
ggplot(dfb,
       aes(
         x = starttime,
         xend = endtime,
         y = label,
         yend = label,
         color = batchid
       )) +
  geom_segment(size = 2) +
  ggtitle(paste0('CT19H29007', '\n', 'start = ', min(dfb$starttime), '\n', 'end = ', max(dfb$endtime)))

dfb <-
  df %>%
  filter(batchid == 'CT19H6007') %>%
  select(label, starttime, endtime, uniqueid, batchid) %>%
  mutate(label = factor(label, levels = rev(unique(label)), ordered  = TRUE)) %>%
  data.frame()
ggplot(dfb,
       aes(
         x = starttime,
         xend = endtime,
         y = label,
         yend = label,
         color = batchid
       )) +
  geom_segment(size = 2) +
  ggtitle(paste0('CT19H6007', '\n', 'start = ', min(dfb$starttime), '\n', 'end = ', max(dfb$endtime)))

dfb <-
  df %>%
  filter(batchid == 'CT19H8002') %>%
  select(label, starttime, endtime, uniqueid, batchid) %>%
  mutate(label = factor(label, levels = rev(unique(label)), ordered  = TRUE)) %>%
  data.frame()
ggplot(dfb,
       aes(
         x = starttime,
         xend = endtime,
         y = label,
         yend = label,
         color = batchid
       )) +
  geom_segment(size = 2) +
  ggtitle(paste0('CT19H8002', '\n', 'start = ', min(dfb$starttime), '\n', 'end = ', max(dfb$endtime)))

dfb <-
  df %>%
  filter(batchid == 'CT19H8004') %>%
  select(label, starttime, endtime, uniqueid, batchid) %>%
  mutate(label = factor(label, levels = rev(unique(label)), ordered  = TRUE)) %>%
  data.frame()
ggplot(dfb,
       aes(
         x = starttime,
         xend = endtime,
         y = label,
         yend = label,
         color = batchid
       )) +
  geom_segment(size = 2) +
  ggtitle(paste0('CT19H8004', '\n', 'start = ', min(dfb$starttime), '\n', 'end = ', max(dfb$endtime)))

dfb <-
  df %>%
  filter(batchid == 'CT19I15002') %>%
  select(label, starttime, endtime, uniqueid, batchid) %>%
  mutate(label = factor(label, levels = rev(unique(label)), ordered  = TRUE)) %>%
  data.frame()
ggplot(dfb,
       aes(
         x = starttime,
         xend = endtime,
         y = label,
         yend = label,
         color = batchid
       )) +
  geom_segment(size = 2) +
  ggtitle(paste0('CT19I15002', '\n', 'start = ', min(dfb$starttime), '\n', 'end = ', max(dfb$endtime)))

dfb <-
  df %>%
  filter(batchid == 'CT19I16008') %>%
  select(label, starttime, endtime, uniqueid, batchid) %>%
  mutate(label = factor(label, levels = rev(unique(label)), ordered  = TRUE)) %>%
  data.frame()
ggplot(dfb,
       aes(
         x = starttime,
         xend = endtime,
         y = label,
         yend = label,
         color = batchid
       )) +
  geom_segment(size = 2) +
  ggtitle(paste0('CT19I16008', '\n', 'start = ', min(dfb$starttime), '\n', 'end = ', max(dfb$endtime)))

dfb <-
  df %>%
  filter(batchid == 'CT19I4007') %>%
  select(label, starttime, endtime, uniqueid, batchid) %>%
  mutate(label = factor(label, levels = rev(unique(label)), ordered  = TRUE)) %>%
  data.frame()
ggplot(dfb,
       aes(
         x = starttime,
         xend = endtime,
         y = label,
         yend = label,
         color = batchid
       )) +
  geom_segment(size = 2) +
  ggtitle(paste0('CT19I4007', '\n', 'start = ', min(dfb$starttime), '\n', 'end = ', max(dfb$endtime)))

dfb <-
  df %>%
  filter(batchid == 'CT19I8001') %>%
  select(label, starttime, endtime, uniqueid, batchid) %>%
  mutate(label = factor(label, levels = rev(unique(label)), ordered  = TRUE)) %>%
  data.frame()
ggplot(dfb,
       aes(
         x = starttime,
         xend = endtime,
         y = label,
         yend = label,
         color = batchid
       )) +
  geom_segment(size = 2) +
  ggtitle(paste0('CT19I8001', '\n', 'start = ', min(dfb$starttime), '\n', 'end = ', max(dfb$endtime)))

###############

dfb <-
  df %>%
  filter(batchid == 'CT19I8001') %>%
  select(label, starttime, endtime, duration, uniqueid, batchid) %>%
  mutate(label = factor(label, levels = rev(unique(label)), ordered  = TRUE)) %>%
  data.frame()
dfb
