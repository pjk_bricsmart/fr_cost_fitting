suppressMessages(library(tidyverse))
suppressMessages(library(magrittr))
suppressMessages(library(usethis))
suppressMessages(library(lubridate))
suppressMessages(library(janitor))
suppressMessages(library(ggplot2))
suppressMessages(library(ggthemes))
suppressMessages(library(ggrepel))
suppressMessages(library(ggpubr))
suppressMessages(library(gridExtra))
suppressMessages(library(here))
suppressMessages(library(vtree))
suppressMessages(library(janitor))

# read data
df <-
  read.csv(here('data', 'FR-MC Batch Values  - 2019-10-01 15.00.37 PM EDT.csv')) %>%
  clean_names()  %>%
  mutate(time = as_datetime(time)) %>%
  data.frame()

# rename: tags >> description
df <- df %>%
  rename(SMBS_FLOW = fi21601_pv) %>%
  rename(TEMPERATURE = ti21631_pv) %>%
  rename(PROGRAM_COMMAND = r216step_sp)

q <- as.character((unique(df$batchid)))

batch <- q[1]

qaz <- df %>%
  filter(batchid == batch) %>%
  select(time, SMBS_FLOW, TEMPERATURE, PROGRAM_COMMAND) %>%
  na.omit() %>%
  data.frame()
scaleFactor <- max(qaz$SMBS_FLOW) / max(qaz$TEMPERATURE)

p2 <- ggplot(qaz, aes(x = time)) +
  geom_line(aes(y = SMBS_FLOW), col = "blue") +
  geom_line(aes(y = TEMPERATURE * scaleFactor), col = "red") +
  scale_y_continuous(name = "SMBS_FLOW",
                     sec.axis = sec_axis(~ . / scaleFactor, name = "TEMPERATURE")) +
  theme(
    axis.title.y.left = element_text(color = "blue"),
    axis.text.y.left = element_text(color = "blue"),
    axis.title.y.right = element_text(color = "red"),
    axis.text.y.right = element_text(color = "red")
  ) +
  ggtitle(batch) +
  ggthemes::theme_hc()

print(p2)
