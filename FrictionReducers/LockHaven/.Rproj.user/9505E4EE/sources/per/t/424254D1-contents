---
title: "Friction Reducers"
subtitle: "Exploratory Data Analysis: FR-MC Batch Values"
author: "P J Kowalczyk"
date: "`r Sys.Date()`"
output:
  pdf_document:
    latex_engine: xelatex
bibliography: skeleton.bib
link-citations: yes
header-includes:
- \usepackage{booktabs}
- \usepackage{longtable}
- \usepackage{array}
- \usepackage{multirow}
- \usepackage{wrapfig}
- \usepackage{float}
- \usepackage{colortbl}
- \usepackage{pdflscape}
- \usepackage{tabu}
- \usepackage{threeparttable}
- \usepackage{threeparttablex}
- \usepackage[normalem]{ulem}
- \usepackage{makecell}
- \usepackage{xcolor}
- \usepackage{rotating}
---

```{r setup, include=FALSE, message = FALSE, warning = FALSE}
suppressMessages(library(tint))
suppressMessages(library(tidyverse))
suppressMessages(library(readxl))
suppressMessages(library(magrittr))
suppressMessages(library(usethis))
suppressMessages(library(lubridate))
suppressMessages(library(janitor))
suppressMessages(library(ggplot2))
suppressMessages(library(ggthemes))
suppressMessages(library(gridExtra))
suppressMessages(library(here))
suppressMessages(library(vtree))
suppressMessages(library(janitor))
suppressMessages(library(naniar))
suppressMessages(library(kableExtra))
# invalidate cache when the package version changes
knitr::opts_chunk$set(tidy = FALSE, cache.extra = packageVersion('tint'), warning = FALSE)
options(htmltools.dir.version = FALSE)
options(knitr.kable.NA = '')
```

```{r read-data, cache = TRUE, include = FALSE}
df <- read.csv(here('data', 'FR-MC Batch Values  - 2019-10-01 15.00.37 PM EDT.csv')) %>%
  clean_names()  %>%
  mutate(timestamp = as_datetime(time)) %>%
  select(timestamp, everything())

# remove rows with ALL entries == NA
rowNA <- apply(df, 1, function(df) all(is.na(df)))
df <- df[ !rowNA, ]
```

# Data

+ The data source for this exploratory analysis is the file: **FR-MC Batch Values  - 2019-10-01 15.00.37 PM EDT.csv**.  
+ The file includes `r dim(df)[1]` observations of `r dim(df)[2]` variables. The variable names are listed in Appendix I.  
+ Data is reported once/minute for the period `r min(df$timestamp)` through `r max(df$timestamp)`  
+ There are `r length(unique(df$batchid))` unique **batchid**'s.  
+ There are `r length(unique(df$r216_step))` unique **r216_step**'s.
\hfill\break

# Exploratory Data Analysis  

## Batchid's & steps  

The leftmost column in Table 1 lists the **batchid**'s present in this dataset. The column headings are the **r216_step**'s in this dataset. The value at the intersection of a row and column represents the number of minutes a particular **batchid** was subjected to a particular **r216_step**. Cells in this table are blank for those instances when a **batchid** was *not* subjected to a particular **r216_step**.  

```{r step_instances, echo = FALSE}
df2 <- df %>%
  group_by(batchid, r216_step) %>%
  summarise(instances = n()) %>%
  spread(r216_step, instances)
# kable(df2)
```

```{r landscape_table, echo = FALSE}
landscape(knitr::kable(df2, "latex", caption = 'Values = number of minutes a batch was subjected to a particular step.') %>%
            row_spec(0, angle = 90) %>%
            kable_styling(font_size = 7)
          )
```

## Gantt Charts

```{r, echo = FALSE}
qaz <- df %>%
  group_by(r216_lot_number, r216_step, r216batch_num_pv, batchid) %>%
  summarise(start = min(timestamp), finish = max(timestamp)) %>%
  data.frame()

qaz <- dplyr::arrange(qaz, start)

# kable(qaz, "latex", longtable = T, booktabs = T) %>%
#   kable_styling(latex_options = "scale_down")

# qaz <- qaz %>% mutate(batchid = factor(batchid), 
#                     batchid = factor(batchid, levels = levels(batchid)))
```

```{r, echo = FALSE}
dfb <-
  qaz %>%
  filter(batchid == '13007') %>%
  select(r216_step, start, finish) %>%
  mutate(r216_step = factor(r216_step, levels = rev(unique(r216_step)), ordered  = TRUE)) %>%
  data.frame()

# convert data to long for ggplot

# dfb_long <- dfb %>%
#   gather('start', 'finish', key = "state", value = "time") %>%
#   mutate(r216_step = factor(r216_step, levels = rev(unique(r216_step)), ordered = TRUE))

```

```{r 13007, echo = FALSE, fig.align = 'center', fig.height = 3, fig.width = 8}
dfb <-
  qaz %>%
  filter(batchid == '13007') %>%
  select(r216_step, start, finish) %>%
  mutate(r216_step = factor(r216_step, levels = rev(unique(r216_step)), ordered  = TRUE)) %>%
  data.frame()
ggplot(dfb, aes(x=start, xend=finish, y=r216_step, yend=r216_step)) +
  geom_segment(size = 2) +
  ggtitle(paste0('13007', '\n', 'start = ', min(dfb$start), '\n', 'end = ', max(dfb$finish)))
```

```{r 14006, echo = FALSE, fig.align = 'center', fig.height = 3, fig.width = 8}
dfb <-
  qaz %>%
  filter(batchid == '14006') %>%
  select(r216_step, start, finish) %>%
  mutate(r216_step = factor(r216_step, levels = rev(unique(r216_step)), ordered  = TRUE)) %>%
  data.frame()
ggplot(dfb, aes(x=start, xend=finish, y=r216_step, yend=r216_step)) +
  geom_segment(size = 2) +
  ggtitle(paste0('14006', '\n', 'start = ', min(dfb$start), '\n', 'end = ', max(dfb$finish)))
```


```{r 20004, echo = FALSE, fig.align = 'center', fig.height = 3, fig.width = 8}
dfb <-
  qaz %>%
  filter(batchid == '20004') %>%
  select(r216_step, start, finish) %>%
  mutate(r216_step = factor(r216_step, levels = rev(unique(r216_step)), ordered  = TRUE)) %>%
  data.frame()
ggplot(dfb, aes(x=start, xend=finish, y=r216_step, yend=r216_step)) +
  geom_segment(size = 2) +
  ggtitle(paste0('20004', '\n', 'start = ', min(dfb$start), '\n', 'end = ', max(dfb$finish)))
```

```{r 24004, echo = FALSE, fig.align = 'center', fig.height = 3, fig.width = 8}
dfb <-
  qaz %>%
  filter(batchid == '24004') %>%
  select(r216_step, start, finish) %>%
  mutate(r216_step = factor(r216_step, levels = rev(unique(r216_step)), ordered  = TRUE)) %>%
  data.frame()
ggplot(dfb, aes(x=start, xend=finish, y=r216_step, yend=r216_step)) +
  geom_segment(size = 2) +
  ggtitle(paste0('24004', '\n', 'start = ', min(dfb$start), '\n', 'end = ', max(dfb$finish)))
```

```{r 24006, echo = FALSE, fig.align = 'center', fig.height = 3, fig.width = 8}
dfb <-
  qaz %>%
  filter(batchid == '24006') %>%
  select(r216_step, start, finish) %>%
  mutate(r216_step = factor(r216_step, levels = rev(unique(r216_step)), ordered  = TRUE)) %>%
  data.frame()
ggplot(dfb, aes(x=start, xend=finish, y=r216_step, yend=r216_step)) +
  geom_segment(size = 2) +
  ggtitle(paste0('24006', '\n', 'start = ', min(dfb$start), '\n', 'end = ', max(dfb$finish)))
```

```{r 27007, echo = FALSE, fig.align = 'center', fig.height = 3, fig.width = 8}
dfb <-
  qaz %>%
  filter(batchid == '27007') %>%
  select(r216_step, start, finish) %>%
  mutate(r216_step = factor(r216_step, levels = rev(unique(r216_step)), ordered  = TRUE)) %>%
  data.frame()
ggplot(dfb, aes(x=start, xend=finish, y=r216_step, yend=r216_step)) +
  geom_segment(size = 2) +
  ggtitle(paste0('27007', '\n', 'start = ', min(dfb$start), '\n', 'end = ', max(dfb$finish)))
```

```{r 28001, echo = FALSE, fig.align = 'center', fig.height = 3, fig.width = 8}
dfb <-
  qaz %>%
  filter(batchid == '28001') %>%
  select(r216_step, start, finish) %>%
  mutate(r216_step = factor(r216_step, levels = rev(unique(r216_step)), ordered  = TRUE)) %>%
  data.frame()
ggplot(dfb, aes(x=start, xend=finish, y=r216_step, yend=r216_step)) +
  geom_segment(size = 2) +
  ggtitle(paste0('28001', '\n', 'start = ', min(dfb$start), '\n', 'end = ', max(dfb$finish)))
```

```{r 28005, echo = FALSE, fig.align = 'center', fig.height = 3, fig.width = 8}
dfb <-
  qaz %>%
  filter(batchid == '28005') %>%
  select(r216_step, start, finish) %>%
  mutate(r216_step = factor(r216_step, levels = rev(unique(r216_step)), ordered  = TRUE)) %>%
  data.frame()
ggplot(dfb, aes(x=start, xend=finish, y=r216_step, yend=r216_step)) +
  geom_segment(size = 2) +
  ggtitle(paste0('28005', '\n', 'start = ', min(dfb$start), '\n', 'end = ', max(dfb$finish)))
```

```{r 28007, echo = FALSE, fig.align = 'center', fig.height = 3, fig.width = 8}
dfb <-
  qaz %>%
  filter(batchid == '28007') %>%
  select(r216_step, start, finish) %>%
  mutate(r216_step = factor(r216_step, levels = rev(unique(r216_step)), ordered  = TRUE)) %>%
  data.frame()
ggplot(dfb, aes(x=start, xend=finish, y=r216_step, yend=r216_step)) +
  geom_segment(size = 2) +
  ggtitle(paste0('28007', '\n', 'start = ', min(dfb$start), '\n', 'end = ', max(dfb$finish)))
```

```{r CT19H10001, echo = FALSE, fig.align = 'center', fig.height = 3, fig.width = 8}
dfb <-
  qaz %>%
  filter(batchid == 'CT19H10001') %>%
  select(r216_step, start, finish) %>%
  mutate(r216_step = factor(r216_step, levels = rev(unique(r216_step)), ordered  = TRUE)) %>%
  data.frame()
ggplot(dfb, aes(x=start, xend=finish, y=r216_step, yend=r216_step)) +
  geom_segment(size = 2) +
  ggtitle(paste0('CT19H10001', '\n', 'start = ', min(dfb$start), '\n', 'end = ', max(dfb$finish)))
```

```{r CT19H12001, echo = FALSE, fig.align = 'center', fig.height = 3, fig.width = 8}
dfb <-
  qaz %>%
  filter(batchid == 'CT19H12001') %>%
  select(r216_step, start, finish) %>%
  mutate(r216_step = factor(r216_step, levels = rev(unique(r216_step)), ordered  = TRUE)) %>%
  data.frame()
ggplot(dfb, aes(x=start, xend=finish, y=r216_step, yend=r216_step)) +
  geom_segment(size = 2) +
  ggtitle(paste0('CT19H12001', '\n', 'start = ', min(dfb$start), '\n', 'end = ', max(dfb$finish)))
```

```{r CT19H12003, echo = FALSE, fig.align = 'center', fig.height = 3, fig.width = 8}
dfb <-
  qaz %>%
  filter(batchid == 'CT19H12003') %>%
  select(r216_step, start, finish) %>%
  mutate(r216_step = factor(r216_step, levels = rev(unique(r216_step)), ordered  = TRUE)) %>%
  data.frame()
ggplot(dfb, aes(x=start, xend=finish, y=r216_step, yend=r216_step)) +
  geom_segment(size = 2) +
  ggtitle(paste0('CT19H12003', '\n', 'start = ', min(dfb$start), '\n', 'end = ', max(dfb$finish)))
```

```{r CT19H14008, echo = FALSE, fig.align = 'center', fig.height = 3, fig.width = 8}
dfb <-
  qaz %>%
  filter(batchid == 'CT19H14008') %>%
  select(r216_step, start, finish) %>%
  mutate(r216_step = factor(r216_step, levels = rev(unique(r216_step)), ordered  = TRUE)) %>%
  data.frame()
ggplot(dfb, aes(x=start, xend=finish, y=r216_step, yend=r216_step)) +
  geom_segment(size = 2) +
  ggtitle(paste0('CT19H14008', '\n', 'start = ', min(dfb$start), '\n', 'end = ', max(dfb$finish)))
```

```{r CT19H29005, echo = FALSE, fig.align = 'center', fig.height = 3, fig.width = 8}
dfb <-
  qaz %>%
  filter(batchid == 'CT19H29005') %>%
  select(r216_step, start, finish) %>%
  mutate(r216_step = factor(r216_step, levels = rev(unique(r216_step)), ordered  = TRUE)) %>%
  data.frame()
ggplot(dfb, aes(x=start, xend=finish, y=r216_step, yend=r216_step)) +
  geom_segment(size = 2) +
  ggtitle(paste0('CT19H29005', '\n', 'start = ', min(dfb$start), '\n', 'end = ', max(dfb$finish)))
```

```{r CT19H29007, echo = FALSE, fig.align = 'center', fig.height = 3, fig.width = 8}
dfb <-
  qaz %>%
  filter(batchid == 'CT19H29007') %>%
  select(r216_step, start, finish) %>%
  mutate(r216_step = factor(r216_step, levels = rev(unique(r216_step)), ordered  = TRUE)) %>%
  data.frame()
ggplot(dfb, aes(x=start, xend=finish, y=r216_step, yend=r216_step)) +
  geom_segment(size = 2) +
  ggtitle(paste0('CT19H29007', '\n', 'start = ', min(dfb$start), '\n', 'end = ', max(dfb$finish)))
```

```{r CT19H6007, echo = FALSE, fig.align = 'center', fig.height = 3, fig.width = 8}
dfb <-
  qaz %>%
  filter(batchid == 'CT19H6007') %>%
  select(r216_step, start, finish) %>%
  mutate(r216_step = factor(r216_step, levels = rev(unique(r216_step)), ordered  = TRUE)) %>%
  data.frame()
ggplot(dfb, aes(x=start, xend=finish, y=r216_step, yend=r216_step)) +
  geom_segment(size = 2) +
  ggtitle(paste0('CT19H6007', '\n', 'start = ', min(dfb$start), '\n', 'end = ', max(dfb$finish)))
```

```{r CT19H8002, echo = FALSE, fig.align = 'center', fig.height = 3, fig.width = 8}
dfb <-
  qaz %>%
  filter(batchid == 'CT19H8002') %>%
  select(r216_step, start, finish) %>%
  mutate(r216_step = factor(r216_step, levels = rev(unique(r216_step)), ordered  = TRUE)) %>%
  data.frame()
ggplot(dfb, aes(x=start, xend=finish, y=r216_step, yend=r216_step)) +
  geom_segment(size = 2) +
  ggtitle(paste0('CT19H8002', '\n', 'start = ', min(dfb$start), '\n', 'end = ', max(dfb$finish)))
```

```{r CT19H8004, echo = FALSE, fig.align = 'center', fig.height = 3, fig.width = 8}
dfb <-
  qaz %>%
  filter(batchid == 'CT19H8004') %>%
  select(r216_step, start, finish) %>%
  mutate(r216_step = factor(r216_step, levels = rev(unique(r216_step)), ordered  = TRUE)) %>%
  data.frame()
ggplot(dfb, aes(x=start, xend=finish, y=r216_step, yend=r216_step)) +
  geom_segment(size = 2) +
  ggtitle(paste0('CT19H8004', '\n', 'start = ', min(dfb$start), '\n', 'end = ', max(dfb$finish)))
```

```{r CT19I15002, echo = FALSE, fig.align = 'center', fig.height = 3, fig.width = 8}
dfb <-
  qaz %>%
  filter(batchid == 'CT19I15002') %>%
  select(r216_step, start, finish) %>%
  mutate(r216_step = factor(r216_step, levels = rev(unique(r216_step)), ordered  = TRUE)) %>%
  data.frame()
ggplot(dfb, aes(x=start, xend=finish, y=r216_step, yend=r216_step)) +
  geom_segment(size = 2) +
  ggtitle(paste0('CT19I15002', '\n', 'start = ', min(dfb$start), '\n', 'end = ', max(dfb$finish)))
```

```{r CT19I16008, echo = FALSE, fig.align = 'center', fig.height = 3, fig.width = 8}
dfb <-
  qaz %>%
  filter(batchid == 'CT19I16008') %>%
  select(r216_step, start, finish) %>%
  mutate(r216_step = factor(r216_step, levels = rev(unique(r216_step)), ordered  = TRUE)) %>%
  data.frame()
ggplot(dfb, aes(x=start, xend=finish, y=r216_step, yend=r216_step)) +
  geom_segment(size = 2) +
  ggtitle(paste0('CT19I16008', '\n', 'start = ', min(dfb$start), '\n', 'end = ', max(dfb$finish)))
```

```{r CT19I4007, echo = FALSE, fig.align = 'center', fig.height = 3, fig.width = 8}
dfb <-
  qaz %>%
  filter(batchid == 'CT19I4007') %>%
  select(r216_step, start, finish) %>%
  mutate(r216_step = factor(r216_step, levels = rev(unique(r216_step)), ordered  = TRUE)) %>%
  data.frame()
ggplot(dfb, aes(x=start, xend=finish, y=r216_step, yend=r216_step)) +
  geom_segment(size = 2) +
  ggtitle(paste0('CT19I4007', '\n', 'start = ', min(dfb$start), '\n', 'end = ', max(dfb$finish)))
```

```{r CT19I8001, echo = FALSE, fig.align = 'center', fig.height = 3, fig.width = 8}
dfb <-
  qaz %>%
  filter(batchid == 'CT19I8001') %>%
  select(r216_step, start, finish) %>%
  mutate(r216_step = factor(r216_step, levels = rev(unique(r216_step)), ordered  = TRUE)) %>%
  mutate(delta_time = (finish - start)/60) %>%
  data.frame()
ggplot(dfb, aes(x=start, xend=finish, y=r216_step, yend=r216_step)) +
  geom_segment(size = 2) +
  ggtitle(paste0('CT19I8001', '\n', 'start = ', min(dfb$start), '\n', 'end = ', max(dfb$finish)))
```

# Appendix
         
## I. Variable Names

```{r, echo = FALSE}
colnames(df)
```

```{r bib, include=FALSE}
# create a bib file for the R packages used in this document
knitr::write_bib(c('base', 'rmarkdown'), file = 'skeleton.bib')
```
