suppressMessages(library(tidyverse))
suppressMessages(library(readxl))
suppressMessages(library(magrittr))
suppressMessages(library(usethis))
suppressMessages(library(lubridate))
suppressMessages(library(janitor))
suppressMessages(library(ggplot2))
suppressMessages(library(ggthemes))
suppressMessages(library(gridExtra))
suppressMessages(library(here))
suppressMessages(library(vtree))
suppressMessages(library(janitor))
suppressMessages(library(naniar))
suppressMessages(library(stringr))
suppressMessages(library(reshape))

##### read data

### R201 FR-HSM

r201_fr_hsm <-
  read.csv(here('data', 'R201_FR_HSM_Batch_Values_17Jan20.csv'),
           stringsAsFactors = FALSE) %>%
  select(TIME, BATCHID) %>%
  mutate(TIME = as_datetime(TIME)) %>%
  group_by(BATCHID) %>%
  summarise(START = min(TIME), END = max(TIME)) %>%
  mutate(DURATION = END - START) %>%
  mutate(SOURCE = 'R201 FR-HSM') %>%
  select(SOURCE, everything())

### R216 FR-HSM

r216_fr_hsm <-
  read.csv(here('data', 'R216_FR_HSM_Batch_Values_17Jan20.csv'),
           stringsAsFactors = FALSE) %>%
  select(TIME, BATCHID) %>%
  mutate(TIME = as_datetime(TIME)) %>%
  group_by(BATCHID) %>%
  summarise(START = min(TIME), END = max(TIME)) %>%
  mutate(DURATION = END - START) %>%
  mutate(SOURCE = 'R216 FR-HSM') %>%
  select(SOURCE, everything())

### R201 S1176 2019

r201_s1176_19 <-
  read.csv(here('data', 'S1176 Batch Values  - 2019-11-07 12.34.37 PM EST.csv'),
           stringsAsFactors = FALSE) %>%
  select(TIME, BATCHID) %>%
  mutate(TIME = as_datetime(TIME)) %>%
  group_by(BATCHID) %>%
  summarise(START = min(TIME), END = max(TIME)) %>%
  mutate(DURATION = END - START) %>%
  mutate(SOURCE = 'R201 S1176') %>%
  select(SOURCE, everything())

### R201 S1176 2020

r201_s1176_20 <-
  read.csv(here('data', 'S1176 Batch Values  - 2020-01-15 13.29.53 PM EST.csv'),
           stringsAsFactors = FALSE) %>%
  select(TIME, BATCHID) %>%
  mutate(TIME = as_datetime(TIME)) %>%
  group_by(BATCHID) %>%
  summarise(START = min(TIME), END = max(TIME)) %>%
  mutate(DURATION = END - START) %>%
  mutate(SOURCE = 'R201 S1176') %>%
  select(SOURCE, everything())

### R216 FR-MC

r216_fr_mc <-
  read.csv(here('data', 'R216 FR-MC Batch Values  - 2019-10-01 15.00.37 PM EDT.csv'),
           stringsAsFactors = FALSE) %>%
  select(TIME, BATCHID) %>%
  mutate(TIME = as_datetime(TIME)) %>%
  group_by(BATCHID) %>%
  summarise(START = min(TIME), END = max(TIME)) %>%
  mutate(DURATION = END - START) %>%
  mutate(SOURCE = 'R216 FR-MC') %>%
  select(SOURCE, everything())

df <- rbind(r201_fr_hsm, r216_fr_hsm, r201_s1176_19, r201_s1176_20, r216_fr_mc) %>%
  distinct(SOURCE, BATCHID, START, END, DURATION, .keep_all = TRUE)

df$DURATION <- round(df$DURATION, digits = 2)

##### all products gannt chart

elapsedTime <- df %>%
  group_by(SOURCE) %>%
  summarise(start = min(START), end = max(END)) %>%
  arrange(start) %>%
  data.frame()

mElapsedTime <- melt(elapsedTime, measure.vars = c('start', 'end'))

ggplot(mElapsedTime, aes(value, SOURCE)) + 
  geom_line(size = 6, color = '#009fe3') +
  xlab(NULL) + 
  ylab(NULL) +
  labs(title = "Source data collection\n2019", color = '#26294d') +
  ggthemes::theme_clean()

##### gannt chart by product

### R216 FR-MC

elapsedTime <- df %>%
  filter(SOURCE == 'R216 FR-MC') %>%
  group_by(BATCHID) %>%
  summarise(start = min(START), end = max(END)) %>%
  arrange(start) %>%
  data.frame()

mElapsedTime <- melt(elapsedTime, measure.vars = c('start', 'end'))

ggplot(mElapsedTime, aes(value, BATCHID)) + 
  geom_line(size = 6, color = '#009fe3') +
  xlab(NULL) + 
  ylab(NULL) +
  labs(title = "R216 FR-MC", color = '#26294d') +
  ggthemes::theme_clean()

### R216 FR-HSM

elapsedTime <- df %>%
  filter(SOURCE == 'R216 FR-HSM') %>%
  group_by(BATCHID) %>%
  summarise(start = min(START), end = max(END)) %>%
  arrange(start) %>%
  data.frame()

mElapsedTime <- melt(elapsedTime, measure.vars = c('start', 'end'))

ggplot(mElapsedTime, aes(value, BATCHID)) + 
  geom_line(size = 6, color = '#009fe3') +
  xlab(NULL) + 
  ylab(NULL) +
  labs(title = "R216 FR-HSM", color = '#26294d') +
  ggthemes::theme_clean()

### R201 S1176

elapsedTime <- df %>%
  filter(SOURCE == 'R201 S1176') %>%
  group_by(BATCHID) %>%
  summarise(start = min(START), end = max(END)) %>%
  arrange(start) %>%
  data.frame()

mElapsedTime <- melt(elapsedTime, measure.vars = c('start', 'end'))

ggplot(mElapsedTime, aes(value, BATCHID)) + 
  geom_line(size = 6, color = '#009fe3') +
  xlab(NULL) + 
  ylab(NULL) +
  labs(title = "R201 S1176", color = '#26294d') +
  ggthemes::theme_clean()

### R201 FR-HSM

elapsedTime <- df %>%
  filter(SOURCE == 'R201 FR-HSM') %>%
  group_by(BATCHID) %>%
  summarise(start = min(START), end = max(END)) %>%
  arrange(start) %>%
  data.frame()

mElapsedTime <- melt(elapsedTime, measure.vars = c('start', 'end'))

ggplot(mElapsedTime, aes(value, BATCHID)) + 
  geom_line(size = 6, color = '#009fe3') +
  xlab(NULL) + 
  ylab(NULL) +
  labs(title = "R201 FR-HSM", color = '#26294d') +
  ggthemes::theme_clean()

##### dotplots

### R201 FR-HSM

ggplot(r201_fr_hsm, aes(x = BATCHID, y = DURATION)) +
  geom_point() +
  geom_segment(aes(
    x = BATCHID,
    xend = BATCHID,
    y = 0,
    yend = DURATION
  )) + coord_flip() +
  labs(title = "R201 FR-HSM", color = '#26294d') + ylab("DURATION (hrs)") +
  ggthemes::theme_clean()

### R216 FR-HSM

ggplot(r216_fr_hsm, aes(x = BATCHID, y = DURATION)) +
  geom_point() +
  geom_segment(aes(
    x = BATCHID,
    xend = BATCHID,
    y = 0,
    yend = DURATION
  )) + coord_flip() +
  labs(title = "R216 FR-HSM", color = '#26294d') + ylab("DURATION (hrs)") +
  ggthemes::theme_clean()

### R201 S1176

ggplot(df %>% filter(SOURCE == 'R201 S1176'), aes(x = BATCHID, y = DURATION)) +
  geom_point() +
  geom_segment(aes(
    x = BATCHID,
    xend = BATCHID,
    y = 0,
    yend = DURATION
  )) + coord_flip() +
  labs(title = "R201 S1176", color = '#26294d') + ylab("DURATION (hrs)") +
  ggthemes::theme_clean()

### R216 FR-MC

ggplot(r216_fr_mc, aes(x = BATCHID, y = DURATION)) +
  geom_point() +
  geom_segment(aes(
    x = BATCHID,
    xend = BATCHID,
    y = 0,
    yend = DURATION
  )) + coord_flip() +
  labs(title = "R216 FR-MC", color = '#26294d') + ylab("DURATION (hrs)") +
  ggthemes::theme_clean()

#####

fl <- read.csv(here('data', 'FlowLoopCurated_2020_01_15.csv'),
               stringsAsFactors = FALSE)
colnames(fl) <- c('FlowLoopSource', 'BATCHID', 'FR_30sec', 'FR_60sec')

alles <- full_join(df, fl)

alles_complete <- alles %>% na.omit

##### 2020-02-10

r201_fr_hsm <-
  read.csv(here('data', 'R201_FR_HSM_Batch_Values_17Jan20.csv'),
           stringsAsFactors = FALSE) %>%
  select(TIME, BATCHID) %>%
  mutate(TIME = as_datetime(TIME)) %>%
  group_by(BATCHID) %>%
  summarise(START = min(TIME), END = max(TIME)) %>%
  mutate(DURATION = END - START) %>%
  mutate(SOURCE = 'R201 FR-HSM') %>%
  select(SOURCE, everything())
r216_fr_hsm <-
  read.csv(here('data', 'R216_FR_HSM_Batch_Values_17Jan20.csv'),
           stringsAsFactors = FALSE) %>%
  select(TIME, BATCHID) %>%
  mutate(TIME = as_datetime(TIME)) %>%
  group_by(BATCHID) %>%
  summarise(START = min(TIME), END = max(TIME)) %>%
  mutate(DURATION = END - START) %>%
  mutate(SOURCE = 'R216 FR-HSM') %>%
  select(SOURCE, everything())
r201_s1176_19 <-
  read.csv(here('data', 'S1176 Batch Values  - 2019-11-07 12.34.37 PM EST.csv'),
           stringsAsFactors = FALSE) %>%
  select(TIME, BATCHID) %>%
  mutate(TIME = as_datetime(TIME)) %>%
  group_by(BATCHID) %>%
  summarise(START = min(TIME), END = max(TIME)) %>%
  mutate(DURATION = END - START) %>%
  mutate(SOURCE = 'R201 S1176') %>%
  select(SOURCE, everything())
r201_s1176_20 <-
  read.csv(here('data', 'S1176 Batch Values  - 2020-01-15 13.29.53 PM EST.csv'),
           stringsAsFactors = FALSE) %>%
  select(TIME, BATCHID) %>%
  mutate(TIME = as_datetime(TIME)) %>%
  group_by(BATCHID) %>%
  summarise(START = min(TIME), END = max(TIME)) %>%
  mutate(DURATION = END - START) %>%
  mutate(SOURCE = 'R201 S1176') %>%
  select(SOURCE, everything())
r216_fr_mc <-
  read.csv(here('data', 'R216 FR-MC Batch Values  - 2019-10-01 15.00.37 PM EDT.csv'),
           stringsAsFactors = FALSE) %>%
  select(TIME, BATCHID) %>%
  mutate(TIME = as_datetime(TIME)) %>%
  group_by(BATCHID) %>%
  summarise(START = min(TIME), END = max(TIME)) %>%
  mutate(DURATION = END - START) %>%
  mutate(SOURCE = 'R216 FR-MC') %>%
  select(SOURCE, everything())

##### Main Reaction

### R216

R216_BATCHIDS_WITH_DATA <- alles_complete %>%
  filter(SOURCE == 'R216 FR-MC') %>%
  select(BATCHID)

R216 <-
  read.csv(here('data', 'R216 FR-MC Batch Values  - 2019-10-01 15.00.37 PM EDT.csv'),
           stringsAsFactors = FALSE) %>%
  mutate(TIME = as_datetime(TIME))

R216_DATA <- left_join(R216_BATCHIDS_WITH_DATA, R216)

table(R216_DATA$BATCHID)

MainRxn <- R216_DATA %>%
  filter(R216_BatchReacting == 'ON')

q <- MainRxn %>%
  group_by(BATCHID) %>%
  summarise(
    MainRxnTmin = min(TI21631.PV),
    MainRxnTmax = max(TI21631.PV),
    MainRxnTavg = mean(TI21631.PV),
    MainRxnTime = difftime(max(TIME), min(TIME), units = "mins")
  )

N2Sparge <- R216_DATA %>%
  filter(R216_BatchSparging == 'ON')

z <- N2Sparge %>%
  group_by(BATCHID) %>%
  summarise(N2SpargeTime = difftime(max(TIME), min(TIME), units = "mins"))

# AqueousCharge <- R216_DATA %>%
#   filter(R216_BatchChargingAqueous == 'ON')
# 
# v <- AqueousCharge %>%
#   group_by(BATCHID) %>%
#   summarise(
#     AqueousChargeTmin = min(TI21631.PV),
#     AqueousChargeTmax = max(TI21631.PV),
#     AqueousChargeTavg = mean(TI21631.PV)
#   )

qzv <- full_join(q, z)
# qzv <- full_join(qzv, v)

### R201

R201_BATCHIDS_WITH_DATA <- alles_complete %>%
  filter(SOURCE == 'R201 S1176') %>%
  select(BATCHID)

R201_19 <-
  read.csv(here('data', 'S1176 Batch Values  - 2019-11-07 12.34.37 PM EST.csv'),
           stringsAsFactors = FALSE) %>%
  mutate(TIME = as_datetime(TIME)) %>%
  select(TIME, BATCHID, TI20121.PV, R201_BatchReacting, R201_BatchSparging)
R201_20 <-
  read.csv(here('data', 'S1176 Batch Values  - 2020-01-15 13.29.53 PM EST.csv'),
           stringsAsFactors = FALSE) %>%
  mutate(TIME = as_datetime(TIME)) %>%
  select(TIME, BATCHID, TI20121.PV, R201_BatchReacting, R201_BatchSparging)

R201 <- rbind(R201_19, R201_20) %>%
  distinct()

R201_DATA <- left_join(R201_BATCHIDS_WITH_DATA, R201)

table(R201_DATA$BATCHID)

MainRxn <- R201_DATA %>%
  filter(R201_BatchReacting == 'ON')

qq <- MainRxn %>%
  group_by(BATCHID) %>%
  summarise(
    MainRxnTmin = min(TI20121.PV),
    MainRxnTmax = max(TI20121.PV),
    MainRxnTavg = mean(TI20121.PV),
    MainRxnTime = difftime(max(TIME), min(TIME), units = "mins")
  )

N2Sparge <- R201_DATA %>%
  filter(R201_BatchSparging == 'ON')

zz <- N2Sparge %>%
  group_by(BATCHID) %>%
  summarise(N2SpargeTime = difftime(max(TIME), min(TIME), units = "mins"))

q2z2 <- full_join(qq, zz)

iop <- rbind(qzv, q2z2)

bnm <- full_join(alles_complete, iop)
