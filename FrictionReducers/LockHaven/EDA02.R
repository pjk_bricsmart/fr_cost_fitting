suppressMessages(library(tint))
suppressMessages(library(tidyverse))
suppressMessages(library(readxl))
suppressMessages(library(magrittr))
suppressMessages(library(usethis))
suppressMessages(library(lubridate))
suppressMessages(library(janitor))
suppressMessages(library(ggplot2))
suppressMessages(library(ggthemes))
suppressMessages(library(ggrepel))
suppressMessages(library(gridExtra))
suppressMessages(library(here))
suppressMessages(library(vtree))
suppressMessages(library(janitor))
suppressMessages(library(naniar))
suppressMessages(library(kableExtra))
suppressMessages(library(stringr))

df <-
  readxl::read_xlsx(here('step_times.xlsx')) %>%
  clean_names()  %>%
  data.frame()

vars <- df %>%
  select(-batch)

df.pca <- prcomp(vars, center = TRUE, scale. = TRUE)

summary(df.pca)

X <- df.pca$x[, "PC1"]
Y <- df.pca$x[, "PC2"]
pts <- cbind(X, Y) %>%
  data.frame()

pts$batch <- df$batch

plot1 <- ggplot2::ggplot(pts, aes(x = X, y = Y)) +
  geom_point(color = 'blue') +
  coord_fixed(xlim = c(-8, 8), ylim = c(-8, 8)) +
  labs(title = 'PCA') + xlab('PCA1 ... 23.3%') + ylab('PC2 ... 22.1%') +
  ggthemes::theme_tufte()
plot1

plot2 <- ggplot2::ggplot(pts, aes(x = X, y = Y, label = batch)) +
  geom_point(color = 'blue') +
  coord_fixed(xlim = c(-8, 8), ylim = c(-8, 8)) +
  geom_text_repel() +
  labs(title = 'PCA') + xlab('PC1 ... 23.3%') + ylab('PC2 ... 22.1%') +
  ggthemes::theme_tufte()
plot2
ggsave(file = "batch_pca_label.jpeg", plot2) #saves gplot2

grid.arrange(plot1, plot2, ncol=2)

#save
g <- arrangeGrob(plot1, plot2, ncol = 2) #generates g
ggsave(file = "batch_pca.jpeg", g) #saves g
