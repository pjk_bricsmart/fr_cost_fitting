---
title: "Friction Reducers"
subtitle: "Exploratory Data Analysis"
author: "Paul J KOWALCZYK"
date: "`r Sys.Date()`"
output:
  tufte::tufte_html: default
  tufte::tufte_handout:
    citation_package: natbib
    latex_engine: xelatex
  tufte::tufte_book:
    citation_package: natbib
    latex_engine: xelatex
bibliography: skeleton.bib
link-citations: yes
---

```{r setup, include = FALSE}
library(tufte)
# invalidate cache when the tufte version changes
knitr::opts_chunk$set(tidy = FALSE, cache.extra = packageVersion('tufte'))
options(htmltools.dir.version = FALSE)
```


```{r, echo = FALSE, eval = FALSE}
file.edit(
  tufte:::template_resources(
    'tufte_html', '..', 'skeleton', 'skeleton.Rmd'
  )
)
```

```{r load_libraries, echo = FALSE, warning = FALSE}
suppressMessages(library(tidyverse))
suppressMessages(library(readxl))
suppressMessages(library(magrittr))
suppressMessages(library(usethis))
suppressMessages(library(here))
suppressMessages(library(janitor))
suppressMessages(library(ggplot2))
suppressMessages(library(ggthemes))
suppressMessages(library(ggrepel))
suppressMessages(library(gridExtra))
suppressMessages(library(cowplot))
```


```{r read_data, echo = FALSE, warning = FALSE, cache = TRUE}
dfBatchID <-
  readxl::read_xlsx(here('Batch OrganizerV2.xlsx'), sheet = 'HSM', range = "A2:A61") %>%
  clean_names()  %>%
  data.frame()
dfVariables <-
  readxl::read_xlsx(here('Batch OrganizerV2.xlsx'), sheet = 'HSM', range = "AX2:BD61") %>%
  clean_names()  %>%
  data.frame()
df <- data.frame(dfBatchID, dfVariables)
```

+ data source: 'Batch OrganizerV2.xlsx'  
+ worksheet = HSM  
+ plot a scatterplot of variable values versus batch_id for each of **turn_1**, **turn_2**, **top_temp**, **smbs_to_turn**, **total_smbs**, **initiate_temp**, and **max_temp**  
+ plot a boxplot for each variable

```{r data_table, echo = FALSE, warning = FALSE, cache = TRUE}
knitr::kable(head(df), caption = 'Sample of data specific to this exploratory data analysis. Data reported in minutes.')
```

**N.B.** For each variable the scatterplot and boxplot are presented side-by-side.

# turn_1
```{r turn_1, echo = FALSE, fig.fullwidth = TRUE}
plot1 <- ggplot2::ggplot(df, aes(x = batch_id, y = turn_1)) +
  geom_point() +
  ggthemes::theme_hc() +
  theme(axis.text.x = element_text(size = 6, angle = 90, vjust = 0.5))

plot2 <- ggplot2::ggplot(df, aes(x = '', y = turn_1)) +
  geom_boxplot() +
  ggthemes::theme_hc() +
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5))

plot_grid(plot1, plot2, align = "h", axis = "b", rel_widths = c(4, 1))
```

# turn_2
```{r turn_2, echo = FALSE, fig.fullwidth = TRUE}
plot1 <- ggplot2::ggplot(df, aes(x = batch_id, y = turn_2)) +
  geom_point() +
  ggthemes::theme_hc() +
  theme(axis.text.x = element_text(size = 6, angle = 90, vjust = 0.5))

plot2 <- ggplot2::ggplot(df, aes(x = '', y = turn_2)) +
  geom_boxplot() +
  ggthemes::theme_hc() +
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5))

plot_grid(plot1, plot2, align = "h", axis = "b", rel_widths = c(4, 1))
```

# top_temp
```{r top_temp, echo = FALSE, fig.fullwidth = TRUE}
plot1 <- ggplot2::ggplot(df, aes(x = batch_id, y = top_temp)) +
  geom_point() +
  ggthemes::theme_hc() +
  theme(axis.text.x = element_text(size = 6, angle = 90, vjust = 0.5))

plot2 <- ggplot2::ggplot(df, aes(x = '', y = top_temp)) +
  geom_boxplot() +
  ggthemes::theme_hc() +
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5))

plot_grid(plot1, plot2, align = "h", axis = "b", rel_widths = c(4, 1))
```

# smbs_to_turn
```{r smbs_to_turn, echo = FALSE, fig.fullwidth = TRUE}
plot1 <- ggplot2::ggplot(df, aes(x = batch_id, y = smbs_to_turn)) +
  geom_point() +
  ggthemes::theme_hc() +
  theme(axis.text.x = element_text(size = 6, angle = 90, vjust = 0.5))

plot2 <- ggplot2::ggplot(df, aes(x = '', y = smbs_to_turn)) +
  geom_boxplot() +
  ggthemes::theme_hc() +
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5))

plot_grid(plot1, plot2, align = "h", axis = "b", rel_widths = c(4, 1))
```

# total_smbs
```{r total_smbs, echo = FALSE, fig.fullwidth = TRUE}
plot1 <- ggplot2::ggplot(df, aes(x = batch_id, y = total_smbs)) +
  geom_point() +
  ggthemes::theme_hc() +
  theme(axis.text.x = element_text(size = 6, angle = 90, vjust = 0.5))

plot2 <- ggplot2::ggplot(df, aes(x = '', y = total_smbs)) +
  geom_boxplot() +
  ggthemes::theme_hc() +
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5))

plot_grid(plot1, plot2, align = "h", axis = "b", rel_widths = c(4, 1))
```

# initiate_temp
```{r initiate_temp, echo = FALSE, fig.fullwidth = TRUE}
plot1 <- ggplot2::ggplot(df, aes(x = batch_id, y = initiate_temp)) +
  geom_point() +
  ggthemes::theme_hc() +
  theme(axis.text.x = element_text(size = 6, angle = 90, vjust = 0.5))

plot2 <- ggplot2::ggplot(df, aes(x = '', y = initiate_temp)) +
  geom_boxplot() +
  ggthemes::theme_hc() +
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5))

plot_grid(plot1, plot2, align = "h", axis = "b", rel_widths = c(4, 1))
```

# max_temp
```{r max_temp, echo = FALSE, fig.fullwidth = TRUE}
plot1 <- ggplot2::ggplot(df, aes(x = batch_id, y = max_temp)) +
  geom_point() +
  ggthemes::theme_hc() +
  theme(axis.text.x = element_text(size = 6, angle = 90, vjust = 0.5))

plot2 <- ggplot2::ggplot(df, aes(x = '', y = max_temp)) +
  geom_boxplot() +
  ggthemes::theme_hc() +
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5))

plot_grid(plot1, plot2, align = "h", axis = "b", rel_widths = c(4, 1))
```


```{r bib, include=FALSE}
# create a bib file for the R packages used in this document
knitr::write_bib(c('base', 'rmarkdown'), file = 'skeleton.bib')
```
