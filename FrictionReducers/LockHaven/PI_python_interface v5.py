
# coding: utf-8

# In[ ]:


import sys
import clr
import pandas
# import matplotlib.pyplot as plt #seem to have error on that line when calling from R (not Python)
# import PIconnect 
import time
import datetime

sys.path.append('C:\\Program Files (x86)\\PIPC\\AF\\PublicAssemblies\\4.0')  
clr.AddReference('OSIsoft.AFSDK')

from OSIsoft.AF import *
from OSIsoft.AF.PI import *
from OSIsoft.AF.Search import * 
from OSIsoft.AF.Asset import *
from OSIsoft.AF.Data import *
from OSIsoft.AF.Time import *
from OSIsoft.AF.UnitsOfMeasure import *

def list_Servers():
    piServers = PIServers() 
    return [piServers[i].Name for i in range(0,piServers.Count)]

def connect_to_Server(serverName):  
    piServers = PIServers()  
    global piServer  
    piServer = piServers[serverName]  
    piServer.Connect(False)
    print ('Connected to server: ' + serverName)

def connect_to_Server_SDK(serverName='USCSTMS01-SV01'):  
    piServers = PIServers()  
    global piServer  
    piServer = piServers[serverName]  
    piServer.Connect(False)
    print ('Connected to server: ' + serverName)
    return(piServer)

## CONNECT TO AF SERVER AND PRINT ATTRIBUTE VALUE
def connect_to_AF(AFserverName, Database, Tech, Plant, Unit, Attribute): 
    afServers = PISystems()  
    afServer = afServers[AFserverName]                                                  #Write AF Server Name
    afServer.Connect()                                                                  #Connect to AF Server
    DB = afServer.Databases.get_Item(Database)                                          #Define architecture
    element = DB.Elements.get_Item(Tech).Elements.get_Item(Plant).Elements.get_Item(Plant + " " + Unit)
    attribute = element.Attributes.get_Item(Attribute)
    attval = attribute.GetValue()
    print ('Element Name: {0}'.format(element.Name))                                    #Print Attributr Value
    print ('Attribute Name: {0} \nValue: {1} \nUOM: {2}'.format(attribute.Name, attval.Value, attribute.DefaultUOM))

## FIND TAGS
def find_tags(mask):
    points = PIPoint.FindPIPoints(piServer, mask, None, None)                           #Select PI Server and Mask
    points = list(points)
    return [print(i.get_Name()) for i in points]                                        #Print coincidences

  
def get_tag_snapshot(tagname):  
    tag = PIPoint.FindPIPoint(piServer, tagname)  
    lastData = tag.Snapshot()  
    return lastData.Value, lastData.Timestamp  

def get_RecordedValues (tagname, timestart, timeend,**kwargs):
    if (isinstance(timestart,int)):
        timestart = float(timestart)
    if (isinstance(timeend,int)):
        timeend = float(timeend)   
    timestart = AFTime(timestart)                                                   # specifies object is a AFtime object
    timeend = AFTime(timeend)
    timerange = AFTimeRange(timestart, timeend)
    pt = PIPoint.FindPIPoint(piServer, tagname)  
    pivalues = pt.RecordedValues(timerange, AFBoundaryType.Inside, "", False)
    df = pivalues_to_df(pivalues,**kwargs)
    return df
    
    ## GET SAMPLED VALUES
def get_interpolated_values(tagname, timestart, timeend, span = '1m',**kwargs):
#    print('before ', timestart)
    if (isinstance(timestart,int)):
        timestart = float(timestart)
    if (isinstance(timeend,int)):
        timeend = float(timeend)   
    timestart = AFTime(timestart)                                                   # specifies object is a AFtime object
    timeend = AFTime(timeend)
    span = AFTimeSpan.Parse(span)
    tag = PIPoint.FindPIPoint(piServer,tagname)                                     #Select PI Server and Tag name
    timerange = AFTimeRange(timestart, timeend)                                     #Select Time Range (Osisoft PI format)
    pivalues = tag.InterpolatedValues(timerange, span, '', False)                   #Get Sampled Values (IMPORTANT: Define Span)
#    print('Showing sampled values in PI Tag {0} from {1} to {2}'.format(tagname,timestart,timeend))                 #Print Sampled Values
#    for event in pivalues:  
#        print('{0} value: {1}'.format(event.Timestamp.LocalTime, event.Value)) 
    df = pivalues_to_df(pivalues,**kwargs)
    return df

def pivalues_to_df (pivalues, timeflag= True):
    timestamps, values = [], []
    for value in pivalues:
        try:
            timestamps.append(float(value.Timestamp.UtcSeconds))
        except:
             timestamps.append(str(value.Timestamp.UtcSeconds))           
        try:
            values.append(float(value.Value))
        except:
            values.append(str(value.Value))
#        print(type(value.Value))
#        if (isinstance(value.Value, object)):
#            values.append(str(value.Value))
#        else:
#            values.append(value.Value)
    if (timeflag):
        Var = { 'TIME' : pandas.Series(timestamps),
                pivalues.PIPoint.Name : pandas.Series(values)}
        df = pandas.DataFrame(Var)
    else:
        df = pandas.DataFrame({pivalues.PIPoint.Name : pandas.Series(values)})
    return df    
    
def get_RecordedValues (tagname, timestart, timestop):
    timerange = AFTimeRange(timestart, timestop)
    pt = PIPoint.FindPIPoint(piServer, tagname)  
    recorded = pt.RecordedValues(timerange, AFBoundaryType.Inside, "", False)
    tVar= []
    vVar=[]
    for event in recorded:
        tVar.append(str(event.Timestamp.LocalTime))
        vVar.append(event.Value)     
    Var = {'TIME' : pandas.Series(tVar),
            tagname : pandas.Series(vVar)}
    df = pandas.DataFrame(Var)
    return df


def get_Batch_interpolated_values(dfbatchlist,taglist, span = '1m'):
    df = pandas.DataFrame()
    if isinstance(taglist, dict) and len(taglist) != 0:  # check if dictionary and not empty
        taglistflag = True
    else:                                               # add some error checking in future to see if list/string and not empty
        taglistflag = False
    print(taglist)

    for row in dfbatchlist.itertuples():
        try:
            if (taglistflag):
                mytaglist = taglist[row.UNIT]
            else:
                mytaglist = taglist
            print('\n retrieving batch data for ', row.BATCHID,row.STARTTIME, row.ENDTIME)
            dfb = pandas.DataFrame()
            bool1 = True
            for tag in mytaglist:
#                print('\n retieving values for ',tag)
                dft = get_interpolated_values(tag,row.STARTTIME,row.ENDTIME,span, timeflag = bool1)
                dfb = pandas.concat([dfb,dft], axis=1)
                bool1 = False
            dfb['BATCHID']= row.BATCHID
            df = pandas.concat([df,dfb])  
            print(tag, row.UNIT)
 #           print(df.iloc[1:2])
        except:
            print('failed to retrieve batch',  row.BATCHID,row.STARTTIME, row.ENDTIME)
    df.index = range(len(df.index))
    return df



def get_tag_values(tag_name, start_date, end_date):
    tag = PIPoint.FindPIPoint(piServer, tag_name)
    range = AFTimeRange(start_date, end_date)
    data_points = list(tag.RecordedValues(range, AFBoundaryType.Inside, '', False, 0))
    tVar= []
    vVar=[]
    for pt in data_points:
        tVar.append(str(pt.Timestamp.LocalTime))
        vVar.append(pt.Value)     
    Var = {'TIME' : pandas.Series(tVar),
            tag_name : pandas.Series(vVar)}
    df = pandas.DataFrame(Var)
    return df

def find_tags(mask):
   points = PIPoint.FindPIPoints(piServer, mask, None, None)
   points = list(points) #casting it to a Python list
   return [i.get_Name() for i in points]


def main(): 
  
    server = 'USPASMS01-SV01'
#    onetag = 'TIC22123/PV'
#    tagnamelist = ['FIC42406/PV',
#                'TIC22123/PV',
#                'TI22151/PV',
#                'TIC22305/PV',
#                'PI22102/PV',
#                'LI22162/PV',
#                'FIC22406/PV',
#                'FIC22407/PV',
#                'FQI22420/TOTAL',
#                'FQIC20310/TOTAL',
#                'FQIC20410/TOTAL',
#                'FQIC20501/TOTAL',
#                'PI22102/PV',
#                'PI22232/PV',
#                'PI22112/PV',
#                'PIC22110/PV',
#                'P22030',
#                'JI22207/PV',
#                'SIC22207/PV']
    
    MyServer = connect_to_Server(server)
    #value = get_tag_snapshot(tagname)
    #for tagname in tagnamelist: 
    #        ListData = get_RecordedValues(tagname, "*-3h", "*") 
    #Data = get_tag_values('TIC22123/PV', "*-3h", "*")
    
#    myval = get_interpolated_values('TIC22123/PV', "*-3h", "*","10m")
    
#    mylist = find_tags('TIC22*')
    # myvalue =PIconnect.PIData.PISeriesContainer.recorded_values('TIC22123/PV', "*-3h", "*","inside","")
    #pt1 =PIconnect.PIData.PISeriesContainer('TIC22123/PV')
    #val1 = pt1.recorded_values('TIC22123/PV', "*-3h","*")
#    val3 = get_tag_values('TIC22123/PV', "*-3h","*")
    
    
#    pt = PIPoint.FindPIPoint(piServer, 'TIC22123/PV')  
    #name = pt.Name()  
    # CurrentValue  
    #print('\nShowing PI Tag CurrentValue from {0}'.format(name))  
#    current_value = pt.Snapshot()  
#    print(current_value)


if __name__ == '__main__':
    main()
