suppressMessages(library(tidyverse))
suppressMessages(library(readxl))
suppressMessages(library(magrittr))
suppressMessages(library(usethis))
suppressMessages(library(lubridate))
suppressMessages(library(janitor))
suppressMessages(library(ggplot2))
suppressMessages(library(ggthemes))
suppressMessages(library(ggrepel))
suppressMessages(library(ggpubr))
suppressMessages(library(gridExtra))
suppressMessages(library(here))
suppressMessages(library(vtree))
suppressMessages(library(janitor))
suppressMessages(library(dplyr))
suppressMessages(library(data.table))

# # read data
# flow_loop <-
#   readxl::read_xlsx(here('data', 'BatchList_FlowLoopData_Curated.xlsx'), sheet = 'Sheet1') %>%
#   filter(Reactor == 'R201') %>%
#   select(Source, Batch, Time_030, Time_060) %>%
#   data.frame()

plant <- 
  read.csv(here('data', 'S1176 Batch Values  - 2019-11-07 12.34.37 PM EST.csv')) %>%
  select(`TIME`, `R201.Lot.Number`, `FI20101.PV`, `FI20101.TOT`, `FI20113.PV`, `R201.dT`, `R201_BatchActive`,
         `R201_BatchCompleting`, `R201_BatchReacting`, `R201_BatchSparging`, `R201_BatchTurns`,
         `R201_OilActive`, `R201_TRANSFER.PV`, `R201COOLING.MO`, `R201CW.dT`, `R201TempRate.PV`,
         `R201TopTemp.SP`, `R201Turn1.SP`, `R201Turn2.SP`, `SIC20131.PV`, `SIC20136.PV`,
         `TI20119.PV`, `TI20121.PV`, `TI20126.PV`, `WI20120.PV`) %>%
  rename('Batch' = `R201.Lot.Number`) %>%
  rename('R201_SMBS_FLOW' = `FI20101.PV`) %>%
  rename('R201_SMBS_ADDITION_TOTALIZER' = `FI20101.TOT`) %>%
  rename('R201_COOLING_WATER_FLOW_RATE' = `FI20113.PV`) %>%
  rename('R201_TEMPERATURE_CHANGE_RATE' = `R201.dT`) %>%
  rename('R201_Batch_Active_Tag_for_PI_Ba_Gen' = `R201_BatchActive`) %>% 
  rename('R201_Batch_Completing_Tag_for_PI_Ba_Gen' = `R201_BatchCompleting`) %>% 
  rename('R201_Batch_Reacting_Tag_for_PI_Ba_Gen'= `R201_BatchReacting`) %>% 
  rename('R201_Batch_Sparge_Tag_for_PI_Ba_Gen' = `R201_BatchSparging`) %>% 
  rename('R201_Batch_Turns_Tag_for_PI_Ba_Gen' = `R201_BatchTurns`) %>% 
  rename('R201_Oil_Change_Tag_for_PI_Ba_Gen' = `R201_OilActive`) %>% 
  rename('R201_TRANSFER_COMMAND' = `R201_TRANSFER.PV`) %>% 
  rename('R201_Cooling_Source' = `R201COOLING.MO`) %>% 
  rename('R201_COOLING_WATER_DELTA_T' = `R201CW.dT`) %>% 
  rename('R201_Temp_Rate_of_Change' = `R201TempRate.PV`) %>% 
  rename('R201_TOP_TEMPERATURE' = `R201TopTemp.SP`) %>% 
  rename('R201_TURN_1_TARGET' = `R201Turn1.SP`) %>% 
  rename('R201_TURN_2_TARGET' = `R201Turn2.SP`) %>% 
  rename('P201A_SMBS_Pump_Speed' = `SIC20131.PV`) %>% 
  rename('R201_AGITATOR_SPEED' = `SIC20136.PV`) %>% 
  rename('R201_CWR_TEMPERATURE' = `TI20119.PV`) %>% 
  rename('R201_TEMPERATURE' = `TI20121.PV`) %>% 
  rename('R201_CWS_TEMPERATURE' = `TI20126.PV`) %>% 
  rename('R201_WEIGHT' = `WI20120.PV`) %>% 
  mutate(TIME = as_datetime(TIME)) %>%
  data.frame()

plant <- plant %>%
  filter(Batch != 'No Data')

plant$Batch <- as.character(plant$Batch)

df <- merge(plant, flow_loop, by = 'Batch')

q <- df %>%
  select(-TIME, -R201_Batch_Active_Tag_for_PI_Ba_Gen, -R201_Batch_Completing_Tag_for_PI_Ba_Gen,
         -R201_Batch_Reacting_Tag_for_PI_Ba_Gen, -R201_Batch_Sparge_Tag_for_PI_Ba_Gen,
         -R201_Oil_Change_Tag_for_PI_Ba_Gen,
         -R201_Batch_Turns_Tag_for_PI_Ba_Gen, -R201_TRANSFER_COMMAND, -R201_Cooling_Source,
         -R201_COOLING_WATER_DELTA_T, -P201A_SMBS_Pump_Speed, -Source) %>%
  group_by(Batch) %>%
  summarise(
    R201_SMBS_FLOW = mean(R201_SMBS_FLOW),
    R201_SMBS_ADDITION_TOTALIZER = mean(R201_SMBS_ADDITION_TOTALIZER),
    R201_COOLING_WATER_FLOW_RATE = mean(R201_COOLING_WATER_FLOW_RATE),
    R201_TEMPERATURE_CHANGE_RATE = mean(R201_TEMPERATURE_CHANGE_RATE),
    R201_Temp_Rate_of_Change = mean(R201_Temp_Rate_of_Change),
    R201_TOP_TEMPERATURE = mean(R201_TOP_TEMPERATURE),
    R201_TURN_1_TARGET = mean(R201_TURN_1_TARGET),
    R201_TURN_2_TARGET = mean(R201_TURN_2_TARGET),
    R201_AGITATOR_SPEED = mean(R201_AGITATOR_SPEED),
    R201_CWR_TEMPERATURE = mean(R201_CWR_TEMPERATURE),
    R201_TEMPERATURE = mean(R201_TEMPERATURE),
    R201_CWS_TEMPERATURE = mean(R201_CWS_TEMPERATURE),
    R201_WEIGHT = mean(R201_WEIGHT),
    Time_030 = mean(Time_030),
    Time_060 = mean(Time_060))

write.csv(q, file = 'alles.csv', row.names = FALSE)

##### 2020-01-13 #####

plant <- 
  read.csv(here('data', 'S1176 Batch Values  - 2019-11-07 12.34.37 PM EST.csv')) %>%
  select(TIME, R201.Lot.Number, R201.STEP) %>%
  filter(R201.Lot.Number != 'No Data') %>%
  mutate(TIME = as_datetime(TIME))

steps <- plant %>%
  group_by(R201.Lot.Number, R201.STEP) %>%
  summarise(start = min(TIME), end = max(TIME)) %>%
  data.frame()

steps <- steps[order(steps$start), ]

steps$deltaTime <- steps$end - steps$start
