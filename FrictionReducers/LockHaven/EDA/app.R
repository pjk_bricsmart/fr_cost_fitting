#
# This is a Shiny web application. You can run the application by clicking
# the 'Run App' button above.
#
# Find out more about building applications with Shiny here:
#
#    http://shiny.rstudio.com/
#

suppressMessages(library(shiny))
suppressMessages(library(tidyverse))
suppressMessages(library(magrittr))
suppressMessages(library(usethis))
suppressMessages(library(lubridate))
suppressMessages(library(janitor))
suppressMessages(library(ggplot2))
suppressMessages(library(ggthemes))
suppressMessages(library(ggrepel))
suppressMessages(library(ggpubr))
suppressMessages(library(gridExtra))
suppressMessages(library(here))
suppressMessages(library(vtree))
suppressMessages(library(janitor))

# read data
df <-
    read.csv(here('data', 'FR-MC Batch Values  - 2019-10-01 15.00.37 PM EDT.csv')) %>%
    clean_names()  %>%
    mutate(time = as_datetime(time)) %>%
    data.frame()

# rename: tags >> description
df <- df %>%
    rename(SMBS_FLOW = fi21601_pv) %>%
    rename(TEMPERATURE = ti21631_pv) %>%
    rename(PROGRAM_COMMAND = r216step_sp)

# Define UI for application that draws a histogram
ui <- fluidPage(# Application title
    titlePanel("Friction Reducers EDA"),
    
    # Sidebar with a slider input for number of bins
    sidebarLayout(sidebarPanel(
        selectInput(
            "batch01",
            "Batch number (01):",
            choices = c(
                '13007',
                '14006',
                '20004',
                '24004',
                '24006',
                '27007',
                '28001',
                '28005',
                '28007',
                'CT19H10001',
                'CT19H12001',
                'CT19H12003',
                'CT19H14008',
                'CT19H29005',
                'CT19H29007',
                'CT19H6007',
                'CT19H8002',
                'CT19H8004',
                'CT19I15002',
                'CT19I16008',
                'CT19I4007',
                'CT19I8001'
            )
        ),
        selectInput(
            "batch02",
            "Batch number (02):",
            choices = c(
                '13007',
                '14006',
                '20004',
                '24004',
                '24006',
                '27007',
                '28001',
                '28005',
                '28007',
                'CT19H10001',
                'CT19H12001',
                'CT19H12003',
                'CT19H14008',
                'CT19H29005',
                'CT19H29007',
                'CT19H6007',
                'CT19H8002',
                'CT19H8004',
                'CT19I15002',
                'CT19I16008',
                'CT19I4007',
                'CT19I8001'
            )
        )
    ),
    
    # Show a plot of the generated distribution
    mainPanel(plotOutput("timePlot01"),
              plotOutput("timePlot02"))))

# Define server logic required to draw a histogram
server <- function(input, output) {
    output$timePlot01 <- renderPlot({
        qaz <- df %>%
            filter(batchid == input$batch01) %>%
            select(time, SMBS_FLOW, TEMPERATURE, PROGRAM_COMMAND) %>%
            na.omit() %>%
            data.frame()
        scaleFactor <- max(qaz$SMBS_FLOW) / max(qaz$TEMPERATURE)
        
        # p2 <- ggplot(qaz, aes(x = time)) +
        ggplot(qaz, aes(x = time)) +
            geom_line(aes(y = SMBS_FLOW), col = "blue") +
            geom_line(aes(y = TEMPERATURE * scaleFactor), col = "red") +
            scale_y_continuous(name = "SMBS_FLOW",
                               sec.axis = sec_axis( ~ . / scaleFactor, name = "TEMPERATURE")) +
            theme(
                axis.title.y.left = element_text(color = "blue"),
                axis.text.y.left = element_text(color = "blue"),
                axis.title.y.right = element_text(color = "red"),
                axis.text.y.right = element_text(color = "red")
            ) +
            ggtitle(input$batch01) +
            ggthemes::theme_hc()
    })
    output$timePlot02 <- renderPlot({
        qaz <- df %>%
            filter(batchid == input$batch02) %>%
            select(time, SMBS_FLOW, TEMPERATURE, PROGRAM_COMMAND) %>%
            na.omit() %>%
            data.frame()
        scaleFactor <- max(qaz$SMBS_FLOW) / max(qaz$TEMPERATURE)
        
        # p2 <- ggplot(qaz, aes(x = time)) +
        ggplot(qaz, aes(x = time)) +
            geom_line(aes(y = SMBS_FLOW), col = "blue") +
            geom_line(aes(y = TEMPERATURE * scaleFactor), col = "red") +
            scale_y_continuous(name = "SMBS_FLOW",
                               sec.axis = sec_axis( ~ . / scaleFactor, name = "TEMPERATURE")) +
            theme(
                axis.title.y.left = element_text(color = "blue"),
                axis.text.y.left = element_text(color = "blue"),
                axis.title.y.right = element_text(color = "red"),
                axis.text.y.right = element_text(color = "red")
            ) +
            ggtitle(input$batch02) +
            ggthemes::theme_hc()
    })
}

# Run the application
shinyApp(ui = ui, server = server)
