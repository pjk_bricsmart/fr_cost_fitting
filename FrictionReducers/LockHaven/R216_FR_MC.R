# Load libraries

suppressMessages(library(tidyverse))
suppressMessages(library(readxl))
suppressMessages(library(magrittr))
suppressMessages(library(usethis))
suppressMessages(library(lubridate))
suppressMessages(library(janitor))
suppressMessages(library(ggplot2))
suppressMessages(library(ggthemes))
suppressMessages(library(gridExtra))
suppressMessages(library(here))
suppressMessages(library(vtree))
suppressMessages(library(janitor))
suppressMessages(library(naniar))
suppressMessages(library(stringr))
suppressMessages(library(reshape))

suppressMessages(library(officer))

R216_pptx <- read_pptx()

# read flow loop data

fl <- read.csv(here('data', 'FlowLoopCurated_2020_01_15.csv'),
               stringsAsFactors = FALSE) %>%
  na.omit()
# head(fl)

# R216 FR-HSM

r216_fr_hsm <-
  read.csv(here('data', 'R216_FR_MC_BatchValues_20191001.csv'),
           stringsAsFactors = FALSE) %>%
  mutate(TIME = as_datetime(time))
dim(r216_fr_hsm)
R216 <- r216_fr_hsm %>%
  select(TIME,
         batchid,
         r216_batch_active, # Overall batch tag
         xv21617_pv, # Oil Charge (valve = open)
         r216_batch_surfactant_charging, # Surfactant Charge
         r216_batch_charging_aqueous, # Aqueous Charge
         r216_batch_sparging, # N2 Sparge
         r216_batch_turns, # Reaching turns (initial temperature ramp)
         r216_batch_reacting, # Main Reaction
         r216_batch_completing, # Completion Test
         xv21627_pv # Transfer start (Valve = open)
  ) %>%
  mutate(nchar = str_length(batchid))
R216 <- R216[ R216$nchar != 0, ] %>%
  select(-nchar) %>%
  na.omit()

write.csv(here('data', 'R216_FR_MC_BatchValues_20191001.csv'),
          stringsAsFactors = FALSE)

# RESULTS <- capture.output(for (i in batchlist) {
#   print(i)
#   batch <- R216 %>%
#     filter(BATCHID == i)
#   print(dim(batch))
#   print('BatchActive')
#   print(table(batch$R216_BatchActive))
#   print('Oil Charge')
#   print(table(batch$XV21617.PV))
#   print('Surfactant Charge')
#   print(table(batch$R216_BatchSurfactantCharging))
#   print('Aqueous Charge')
#   print(table(batch$R216_BatchChargingAqueous))
#   print('N2 Sparge')
#   print(table(batch$R216_BatchSparging))
#   print('Reaching turns')
#   print(table(batch$R216_BatchTurns))
#   print('Main Reaction')
#   print(table(batch$R216_BatchReacting))
#   print('Completion Test')
#   print(table(batch$R216_BatchCompleting))
#   print('Transfer start')
#   print(table(batch$XV21627.PV))
# })
# 
# cat("R216", RESULTS, file="R216_RESULTS.txt", sep="\n", append=TRUE)

batchlist <- unique(R216$batchid)

for (i in batchlist) {
  qaz <- R216 %>%
    filter(batchid == i) %>%
    select(-batchid)
  
  alpha <- qaz %>%
    select(
      TIME,
      r216_batch_active,
      xv21617_pv,
      r216_batch_surfactant_charging,
      r216_batch_charging_aqueous,
      r216_batch_sparging,
      r216_batch_turns,
      r216_batch_reacting,
      r216_batch_completing,
      xv21627_pv
    )
  alpha$r216_batch_active <-
    ifelse(alpha$r216_batch_active == 'ON', 1, 0)
  alpha$xv21617_pv <- ifelse(alpha$xv21617_pv == 'OPEN', 2, 0)
  alpha$r216_batch_surfactant_charging <-
    ifelse(alpha$r216_batch_surfactant_charging == 'ON', 3, 0)
  alpha$r216_batch_charging_aqueous <-
    ifelse(alpha$r216_batch_charging_aqueous == 'ON', 4, 0)
  alpha$r216_batch_sparging <-
    ifelse(alpha$r216_batch_sparging == 'ON', 5, 0)
  alpha$r216_batch_turns <-
    ifelse(alpha$r216_batch_turns == 'ON', 6, 0)
  alpha$r216_batch_reacting <-
    ifelse(alpha$r216_batch_reacting == 'ON', 7, 0)
  alpha$r216_batch_completing <-
    ifelse(alpha$r216_batch_completing == 'ON', 8, 0)
  alpha$xv21627_pv <- ifelse(alpha$xv21627_pv == 'OPEN', 9, 0)
  
  beta <-
    alpha %>% gather(Stage, Y, r216_batch_active:xv21627_pv) %>%
    filter(Y > 0)
  
  # plot(beta$TIME, beta$Y)
  
  z <- fl %>%
    filter(batchid == i)
  
  if(!identical(z$X30sec, numeric(0))){
    flowloop <- paste0(' 30sec: ', z$X30sec, '; 60sec: ', z$X60sec)
  } else {
    flowloop <- 'No flow loop data.'
  }
  
  g <- ggplot2::ggplot(beta, aes(x = TIME, y = Y)) +
    geom_point() +
    scale_y_continuous(
      name = "Stage",
      breaks = c(1, 2, 3, 4, 5, 6, 7, 8, 9),
      label = c(
        'Batch Active',
        'Oil Charge',
        'Surfactant Charge',
        'Aqueous Charge',
        'N2 Sparge',
        'Reaching Turns',
        'Main Reaction',
        'Completion Test',
        'Transfer Start'
      )
    ) +
    labs(title = i, subtitle = flowloop) +
    ggthemes::theme_fivethirtyeight()
  
  print(g)
  
  R216_pptx <- add_slide(R216_pptx)
  R216_pptx <- ph_with(R216_pptx, value = g, 
                       location = ph_location_fullsize() )
}

print(R216_pptx, target = "R216.pptx")

