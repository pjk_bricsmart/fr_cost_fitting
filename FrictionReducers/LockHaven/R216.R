# Load libraries

suppressMessages(library(tidyverse))
suppressMessages(library(readxl))
suppressMessages(library(magrittr))
suppressMessages(library(usethis))
suppressMessages(library(lubridate))
suppressMessages(library(janitor))
suppressMessages(library(ggplot2))
suppressMessages(library(ggthemes))
suppressMessages(library(gridExtra))
suppressMessages(library(here))
suppressMessages(library(vtree))
suppressMessages(library(janitor))
suppressMessages(library(naniar))
suppressMessages(library(stringr))
suppressMessages(library(reshape))

# R216 FR-HSM

r216_fr_hsm <-
  read.csv(here('data', 'R216_FR_HSM_Batch_Values_17Jan20.csv'),
           stringsAsFactors = FALSE) %>%
  mutate(TIME = as_datetime(TIME))
dim(r216_fr_hsm)
R216 <- r216_fr_hsm %>%
  select(TIME,
         BATCHID,
         R216_BatchActive, # Overall batch tag
         XV21617.PV, # Oil Charge (valve = open)
         R216_BatchSurfactantCharging, # Surfactant Charge
         R216_BatchChargingAqueous, # Aqueous Charge
         R216_BatchSparging, # N2 Sparge
         R216_BatchTurns, # Reaching turns (initial temperature ramp)
         R216_BatchReacting, # Main Reaction
         R216_BatchCompleting, # Completion Test
         XV21627.PV # Transfer start (Valve = open)
  ) %>%
  mutate(nchar = str_length(BATCHID))
R216 <- R216[ R216$nchar != 0, ] %>%
  select(-nchar) %>%
  na.omit()

RESULTS <- capture.output(for (i in batchlist) {
  print(i)
  batch <- R216 %>%
    filter(BATCHID == i)
  print(dim(batch))
  print('BatchActive')
  print(table(batch$R216_BatchActive))
  print('Oil Charge')
  print(table(batch$XV21617.PV))
  print('Surfactant Charge')
  print(table(batch$R216_BatchSurfactantCharging))
  print('Aqueous Charge')
  print(table(batch$R216_BatchChargingAqueous))
  print('N2 Sparge')
  print(table(batch$R216_BatchSparging))
  print('Reaching turns')
  print(table(batch$R216_BatchTurns))
  print('Main Reaction')
  print(table(batch$R216_BatchReacting))
  print('Completion Test')
  print(table(batch$R216_BatchCompleting))
  print('Transfer start')
  print(table(batch$XV21627.PV))
})

cat("R216", RESULTS, file="R216_RESULTS.txt", sep="\n", append=TRUE)

batchlist <- unique(R216$BATCHID)

for (i in batchlist) {
  qaz <- R216 %>%
    filter(BATCHID == i) %>%
    select(-BATCHID)
  
  alpha <- qaz %>%
    select(
      TIME,
      R216_BatchActive,
      XV21617.PV,
      R216_BatchSurfactantCharging,
      R216_BatchChargingAqueous,
      R216_BatchSparging,
      R216_BatchTurns,
      R216_BatchReacting,
      R216_BatchCompleting,
      XV21627.PV
    )
  alpha$R216_BatchActive <-
    ifelse(alpha$R216_BatchActive == 'ON', 1, 0)
  alpha$XV21617.PV <- ifelse(alpha$XV21617.PV == 'OPEN', 2, 0)
  alpha$R216_BatchSurfactantCharging <-
    ifelse(alpha$R216_BatchSurfactantCharging == 'ON', 3, 0)
  alpha$R216_BatchChargingAqueous <-
    ifelse(alpha$R216_BatchChargingAqueous == 'ON', 4, 0)
  alpha$R216_BatchSparging <-
    ifelse(alpha$R216_BatchSparging == 'ON', 5, 0)
  alpha$R216_BatchTurns <-
    ifelse(alpha$R216_BatchTurns == 'ON', 6, 0)
  alpha$R216_BatchReacting <-
    ifelse(alpha$R216_BatchReacting == 'ON', 7, 0)
  alpha$R216_BatchCompleting <-
    ifelse(alpha$R216_BatchCompleting == 'ON', 8, 0)
  alpha$XV21627.PV <- ifelse(alpha$XV21627.PV == 'OPEN', 9, 0)
  
  beta <-
    alpha %>% gather(Stage, Y, R216_BatchActive:XV21627.PV) %>%
    filter(Y > 0)
  
  # plot(beta$TIME, beta$Y)
  
  g <- ggplot2::ggplot(beta, aes(x = TIME, y = Y)) +
    geom_point() +
    scale_y_continuous(
      name = "Stage",
      breaks = c(1, 2, 3, 4, 5, 6, 7, 8, 9),
      label = c(
        'Batch Active',
        'Oil Charge',
        'Surfactant Charge',
        'Aqueous Charge',
        'N2 Sparge',
        'Reaching Turns',
        'Main Reaction',
        'Completion Test',
        'Transfer Start'
      )
    ) +
    ggtitle(i) +
    ggthemes::theme_fivethirtyeight()
  
  print(g)
}
