---
title: "Lock Haven"
subtitle: "Exploratory Data Analysis"
author: "Paul J Kowalczyk"
date: "`r Sys.Date()`"
output: html_notebook
---

```{r setup, include=FALSE, message = FALSE, warning = FALSE}
suppressMessages(library(tint))
suppressMessages(library(tidyverse))
suppressMessages(library(readxl))
suppressMessages(library(magrittr))
suppressMessages(library(usethis))
suppressMessages(library(lubridate))
suppressMessages(library(janitor))
suppressMessages(library(ggplot2))
suppressMessages(library(ggthemes))
suppressMessages(library(gridExtra))
suppressMessages(library(here))
suppressMessages(library(vtree))
suppressMessages(library(janitor))
suppressMessages(library(naniar))
suppressMessages(library(kableExtra))
# invalidate cache when the package version changes
knitr::opts_chunk$set(tidy = FALSE, cache.extra = packageVersion('tint'), warning = FALSE)
options(htmltools.dir.version = FALSE)
options(knitr.kable.NA = '')
```

```{r read-data, cache = TRUE, include = FALSE}
df <-
  read.csv(here('data', 'FR_MC_Batch_Steps_2019_10_01.csv')) %>% 
  clean_names()  %>%
  mutate(starttime = as_datetime(starttime)) %>%
  mutate(endtime = as_datetime(endtime)) %>%
  arrange(starttime) %>%
  data.frame()

# remove rows with ALL entries == NA
rowNA <- apply(df, 1, function(df) all(is.na(df)))
df <- df[ !rowNA, ]
```

```{r}
dfb <-
  df %>%
  filter(batchid == '13007') %>%
  select(procedure, starttime, endtime, uniqueid, batchid) %>%
  mutate(procedure = factor(procedure, levels = rev(unique(procedure)), ordered  = TRUE)) %>%
  data.frame()
ggplot(dfb,
       aes(
         x = starttime,
         xend = endtime,
         y = procedure,
         yend = procedure,
         color = batchid
       )) +
  geom_segment(size = 2) # +
# ggtitle(paste0('27007', '\n', 'start = ', min(dfb$start), '\n', 'end = ', max(dfb$finish)))
```

```{r}
dfb <-
  df %>%
  filter(batchid == '14006') %>%
  select(procedure, starttime, endtime, uniqueid, batchid) %>%
  mutate(procedure = factor(procedure, levels = rev(unique(procedure)), ordered  = TRUE)) %>%
  data.frame()
ggplot(dfb,
       aes(
         x = starttime,
         xend = endtime,
         y = procedure,
         yend = procedure,
         color = batchid
       )) +
  geom_segment(size = 2) # +
# ggtitle(paste0('27007', '\n', 'start = ', min(dfb$start), '\n', 'end = ', max(dfb$finish)))
```
