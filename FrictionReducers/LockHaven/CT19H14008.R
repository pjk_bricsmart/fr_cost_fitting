suppressMessages(library(tidyverse))
suppressMessages(library(magrittr))
suppressMessages(library(usethis))
suppressMessages(library(lubridate))
suppressMessages(library(janitor))
suppressMessages(library(ggplot2))
suppressMessages(library(ggthemes))
suppressMessages(library(ggrepel))
suppressMessages(library(ggpubr))
suppressMessages(library(gridExtra))
suppressMessages(library(here))
suppressMessages(library(vtree))
suppressMessages(library(janitor))

# read data
df <-
  read.csv(here('data', 'FR-MC Batch Values  - 2019-10-01 15.00.37 PM EDT.csv')) %>%
  clean_names()  %>%
  mutate(time = as_datetime(time)) %>%
  data.frame()

# rename: tags >> description
df <- df %>%
  rename(SMBS_FLOW = fi21601_pv) %>%
  rename(TEMPERATURE = ti21631_pv) %>%
  rename(PROGRAM_COMMAND = r216step_sp)

batch = 'CT19H14008'

qaz <- df %>%
  filter(batchid == batch) %>%
  select(time, SMBS_FLOW, TEMPERATURE, PROGRAM_COMMAND) %>%
  na.omit() %>%
  data.frame()
scaleFactor <- max(qaz$SMBS_FLOW) / max(qaz$TEMPERATURE)

p2 <- ggplot(qaz, aes(x = time)) +
  geom_line(aes(y = SMBS_FLOW), col = "blue") +
  geom_line(aes(y = TEMPERATURE * scaleFactor), col = "red") +
  scale_y_continuous(name = "SMBS_FLOW",
                     sec.axis = sec_axis(~ . / scaleFactor, name = "TEMPERATURE")) +
  theme(
    axis.title.y.left = element_text(color = "blue"),
    axis.text.y.left = element_text(color = "blue"),
    axis.title.y.right = element_text(color = "red"),
    axis.text.y.right = element_text(color = "red")
  ) +
  ggtitle(batch) +
  ggthemes::theme_hc()

p2

ggsave('graphic/CT19H14008_01.png', p2)

for (i in seq(nrow(qaz) - 1)) {
  if ((qaz[(i + 1), 'SMBS_FLOW'] - qaz[i, 'SMBS_FLOW']) > 1.0) {
    print(i + 1)
    print(qaz[(i + 1), 'TEMPERATURE'])
    break
  }
}

deltas <-
  which(qaz$PROGRAM_COMMAND != dplyr::lag(qaz$PROGRAM_COMMAND))
deltas <- c(1, deltas)

ddf <- qaz[deltas,]

ddf$y <- max(ddf$SMBS_FLOW)

p2 <-
  p2 + geom_point(data = ddf, aes(x = time, y = y), size = 0.125) +
  geom_label_repel(data = ddf, aes(x = time, y = y, label = PROGRAM_COMMAND)) +
  geom_vline(
    xintercept = ddf[1, 'time'],
    linetype = "solid",
    color = "orange",
    size = 1.0
  ) + geom_vline(
    xintercept = ddf[2, 'time'],
    linetype = "solid",
    color = "orange",
    size = 1.0
  ) + geom_vline(
    xintercept = ddf[3, 'time'],
    linetype = "solid",
    color = "orange",
    size = 1.0
  ) + geom_vline(
    xintercept = ddf[4, 'time'],
    linetype = "solid",
    color = "orange",
    size = 1.0
  )

p2

ggsave('graphic/CT19H14008_02.png', p2)
