---
title: "Friction Reducers"
subtitle: "EDA"
author: "Paul J. Kowalczyk"
date: "`r Sys.Date()`"
output:
  tufte::tufte_html: default
---

```{r setup, include=FALSE, message = FALSE, warning = FALSE}
suppressMessages(library(tint))
suppressMessages(library(tidyverse))
suppressMessages(library(readxl))
suppressMessages(library(magrittr))
suppressMessages(library(usethis))
suppressMessages(library(lubridate))
suppressMessages(library(janitor))
suppressMessages(library(ggplot2))
suppressMessages(library(ggthemes))
suppressMessages(library(gridExtra))
suppressMessages(library(here))
suppressMessages(library(vtree))
suppressMessages(library(janitor))
suppressMessages(library(naniar))
suppressMessages(library(kableExtra))
suppressMessages(library(formatR))
# invalidate cache when the package version changes
knitr::opts_chunk$set(tidy = FALSE, cache.extra = packageVersion('tint'), warning = FALSE)
options(htmltools.dir.version = FALSE)
options(knitr.kable.NA = '')
```

```{r read-data, cache = TRUE, include = FALSE}
df <- read.csv(here('data', 'FR-MC Batch Values  - 2019-10-01 15.00.37 PM EDT.csv')) %>%
  clean_names()  %>%
  mutate(timestamp = as_datetime(time)) %>%
  select(timestamp, everything())

# remove rows with ALL entries == NA
rowNA <- apply(df, 1, function(df) all(is.na(df)))
df <- df[ !rowNA, ]
```

```{r, cache = TRUE, include = FALSE}
batches <- df %>%
  group_by(r216batch_num_pv) %>%
  summarise(`Start time` = min(timestamp), `End time` = max(timestamp)) %>%
  arrange(`Start time`) %>%
  rename(`Batch` = `r216batch_num_pv`) %>%
  mutate(Duration = format(as.numeric((`End time` - `Start time`)/60), digits=2, nsmall=2, trim = FALSE)) #%>%
  #data.frame()
```

# Batches

```{r, cache = FALSE, echo = FALSE}
knitr::kable(batches, caption = 'Start time, end time, and duration (in minutes) for each batch record.', align = c('rrrr'))
```

## SMBS FLOW  

```{r, cache = FALSE, echo = FALSE, fig.fullwidth = TRUE}
p <- ggplot(df, aes(as.factor(r216batch_num_pv), fi21601_pv))
p + geom_boxplot() + coord_flip() + ggthemes::theme_hc() + labs(x = 'Batch', y = 'SMBS FLOW')
```

## OIL FLOW  

```{r, cache = FALSE, echo = FALSE, fig.fullwidth = TRUE}
p <- ggplot(df, aes(as.factor(r216batch_num_pv), fi21603_pv))
p + geom_boxplot() + coord_flip() + ggthemes::theme_hc() + labs(x = 'Batch', y = 'OIL FLOW')
```

## PRESSURE 

```{r, cache = FALSE, echo = FALSE, fig.fullwidth = TRUE}
p <- ggplot(df, aes(as.factor(r216batch_num_pv), pi21615_pv))
p + geom_boxplot() + coord_flip() + ggthemes::theme_hc() + labs(x = 'Batch', y = 'PRESSURE')
```

## TEMPERATURE

```{r, cache = FALSE, echo = FALSE, fig.fullwidth = TRUE}
p <- ggplot(df, aes(as.factor(r216batch_num_pv), ti21631_pv))
p + geom_boxplot() + coord_flip() + ggthemes::theme_hc() + labs(x = 'Batch', y = 'TEMPERATURE')
```

## WEIGHT

```{r, cache = FALSE, echo = FALSE, fig.fullwidth = TRUE}
p <- ggplot(df, aes(as.factor(r216batch_num_pv), wi21620_pv))
p + geom_boxplot() + coord_flip() + ggthemes::theme_hc() + labs(x = 'Batch', y = 'WEIGHT')
```

# Batch 16008

```{r, cache = FALSE, echo = FALSE, fig.fullwidth = TRUE}
batch_16008 <- df %>%
  filter(r216batch_num_pv == '16008')

p <- ggplot(batch_16008, aes(x = timestamp, y = fi21601_pv, color = r216_step))
p + geom_line() + ggthemes::theme_hc() + labs(x = 'Time', y = 'SMBS FLOW')
```

