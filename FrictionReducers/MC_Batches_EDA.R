# instantiate environment
library(tidyverse)
library(readxl)
library(magrittr)
library(usethis)
library(lubridate)
library(janitor)
library(ggplot2)
library(ggthemes)
library(gridExtra)
library(here)
library(vtree)
library(janitor)
library(naniar)
library(cdparcoord)

here()

df <- readxl::read_xlsx(here('cache', 'MC BatchesV2.xlsx')) %>%
  clean_names() %>%
  rename("mark_invert_pct_over_charge" = "percent_over_charge_29") %>%
  rename("marc_tank_pct_over_charge" = "percent_over_charge_27") %>%
  rename("water_pct_over_charge" = "percent_over_charge_25") %>%
  rename("brine_pct_over_charge" = "percent_over_charge_23") %>%
  rename("oil_pct_over_charge" = "percent_over_charge_21")

# remove rows with ALL entries == NA
rowNA <- apply(df, 1, function(df) all(is.na(df)))
df <- df[ !rowNA, ]

gg_miss_var(df)

vtree(df,"product reactor invert_tank",summary="invert_base_weight \nmean score: %mean%",sameline=TRUE)

gg_miss_var(df)

pcData <- df %>%
  select(product, reactor, invert_tank, invert_base_weight, percent_off_of_batch_sheet_8,
         invert_dilution_weight, percent_off_of_batch_sheet_10, difference, quality_30_sec, quality_1_min)
pcDataDisc <- discretize(pcData, nlevels = 10)
discparcoord(pcDataDisc,k=150)

### reactor = R-216

df206 <- df %>%
  filter(reactor == 'R-216')

cambodia = data.frame(
  Period = c("Funan", "Chenla/Zhenla", "Khmer Empire", "Dark Ages of Cambodia"),
  StartDate = c(-500, 550, 802, 1431),
  EndDate = c(550, 802, 1431, 1863),
  Color = c("lightblue", "lightgreen", "lightyellow", "pink")
)
g2 <- ggplot() +
  geom_segment(data=cambodia, aes(x=StartDate, xend=EndDate, y=Period, yend=Period, color=Color), linetype=1, size=2) +
  scale_colour_brewer(palette = "Pastel1")+
  xlab("Time")+
  ylab("Periods of History")+
  theme_bw() + theme(panel.grid.minor = element_blank(), panel.grid.major =   element_blank()) + theme(aspect.ratio = .2)
g2 + theme(legend.position="none")

df_216StartStop <- read.csv('R-216_Lots.csv')

df_216StartStop$start_time <- lubridate::mdy_hm(df_216StartStop$start_time)
df_216StartStop$end_time <- lubridate::mdy_hm(df_216StartStop$end_time)

g2 <- ggplot() +
  geom_segment(
    data = df_216StartStop,
    aes(
      x = start_time,
      xend = end_time,
      y = lot_number,
      yend = lot_number,
      color = color
    ),
    linetype = 1,
    size = 2,
    inherit.aes = FALSE
  ) +
  # scale_colour_brewer(palette = "Pastel1") +
  xlab("Time Stamp") +
  ylab("Lot Number") + 
  ggthemes::theme_tufte() +
  theme(aspect.ratio = .5) +
  theme(legend.position = "none") +
  theme(axis.text.x = element_text(angle = 90, hjust = 1)) # +
  # ggthemes::theme_tufte()
g2

    