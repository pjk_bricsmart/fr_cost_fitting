# instantiate environment
library(tidyverse)
library(readxl)
library(magrittr)
library(usethis)
library(lubridate)
library(janitor)
library(ggplot2)
library(ggthemes)
library(gridExtra)

R201 <- readRDS('R201.RData')

# plot raw data: value x date

pres_raw <- ggplot(R201, aes(x = date_time, y = r201_201pres)) +
  geom_line() +
  labs(title = 'pres')
pres_raw

msecs_raw <- ggplot(R201, aes(x = date_time, y = msecs)) +
  geom_line() +
  labs(title = 'msecs')
msecs_raw

temp_raw <- ggplot(R201, aes(x = date_time, y = r201_201temp)) +
  geom_line() +
  labs(title = 'temp')
temp_raw

total_raw <- ggplot(R201, aes(x = date_time, y = r201_201total)) +
  geom_line() +
  labs(title = 'total')
total_raw

chillin_raw <- ggplot(R201, aes(x = date_time, y = r201_chillin)) +
  geom_line() +
  labs(title = 'chillin')
chillin_raw

chillout_raw <- ggplot(R201, aes(x = date_time, y = r201_chillout)) +
  geom_line() +
  labs(title = 'chillout')
chillout_raw

coolflow_raw <- ggplot(R201, aes(x = date_time, y = r201_coolflow)) +
  geom_line() +
  labs(title = 'coolflow')
coolflow_raw

smbsflow_raw <- ggplot(R201, aes(x = date_time, y = r201_smbsflow)) +
  geom_line() +
  labs(title = 'smbsflow')
smbsflow_raw

# 5-minute intervals

R201$intervals <- floor_date(R201$date_time, unit = "5minutes")

R201_5mins <- R201 %>%
  select(-date, -time, -date_time) %>%
  group_by(intervals) %>%
  summarise_all(list(mean))

msecs_5min <- ggplot(R201_5mins, aes(x = intervals, y = msecs)) +
  geom_line() +
  labs(title = 'msecs, averaged over 5 minute intervals', x = 'date')
msecs_5min
ggsave("figures/msecs_5min.png")

pres_5min <- ggplot(R201_5mins, aes(x = intervals, y = r201_201pres)) +
  geom_line() +
  labs(title = 'pres, averaged over 5 minute intervals', x = 'date')
pres_5min
ggsave("figures/pres_5min.png")

temp_5min <- ggplot(R201_5mins, aes(x = intervals, y = r201_201temp)) +
  geom_line() +
  labs(title = 'temp, averaged over 5 minute intervals', x = 'date')
temp_5min
ggsave("figures/temp_5min.png")

total_5min <- ggplot(R201_5mins, aes(x = intervals, y = r201_201total)) +
  geom_line() +
  labs(title = 'total, averaged over 5 minute intervals', x = 'date')
total_5min
ggsave("figures/total_5min.png")

chillin_5min <- ggplot(R201_5mins, aes(x = intervals, y = r201_chillin)) +
  geom_line() +
  labs(title = 'chillin, averaged over 5 minute intervals', x = 'date')
chillin_5min
ggsave("figures/chillin_5min.png")
       
chillout_5min <- ggplot(R201_5mins, aes(x = intervals, y = r201_chillout)) +
  geom_line() +
  labs(title = 'chillout, averaged over 5 minute intervals', x = 'date')
chillout_5min
ggsave("figures/chillout_5min.png")

coolflow_5min <- ggplot(R201_5mins, aes(x = intervals, y = r201_coolflow)) +
  geom_line() +
  labs(title = 'coolflow, averaged over 5 minute intervals', x = 'date')
coolflow_5min
ggsave("figures/coolflow_5min.png")

smbsflow_5min <- ggplot(R201_5mins, aes(x = intervals, y = r201_smbsflow)) +
  geom_line() +
  labs(title = 'smbsflow, averaged over 5 minute intervals', x = 'date')
smbsflow_5min
ggsave("figures/smbsflow_5min.png")


grid.arrange(pres_raw, pres_5min, ncol = 1)
grid.arrange(temp_raw, temp_5min, ncol = 1)
grid.arrange(total_raw, total_5min, ncol = 1)
grid.arrange(chillin_raw, chillin_5min, ncol = 1)
grid.arrange(chillout_raw, chillout_5min, ncol = 1)
grid.arrange(coolflow_raw, coolflow_5min, ncol = 1)
grid.arrange(smbsflow_raw, smbsflow_5min, ncol = 1)
grid.arrange(msecs_raw, msecs_5min, ncol = 1)

####

R201 <- readRDS('R201.RData')
R201$minutes <- floor_date(R201$date_time, unit = "5minutes")
R201$hourly <- floor_date(R201$date_time, unit = "1hour")
R201$daily <- floor_date(R201$date_time, unit = "1day")
R201$weekly <- floor_date(R201$date_time, unit = "1week")

##### minutes

minutes <- R201 %>%
  select(-date, -time, -date_time, -hourly, -daily, -weekly) %>%
  group_by(minutes) %>%
  summarise_all(list(mean))

msecs_min <- ggplot(minutes, aes(x = minutes, y = msecs)) +
  geom_line() +
  labs(title = 'msecs, averaged over 5 minute intervals', x = 'date')
msecs_min
ggsave("figures/msecs_min.png")

temp_min <- ggplot(minutes, aes(x = minutes, y = r201_201temp)) +
  geom_line() +
  labs(title = 'temp, averaged over 5 minute intervals', x = 'date')
temp_min
ggsave("figures/temp_min.png")

total_min <- ggplot(minutes, aes(x = minutes, y = r201_201total)) +
  geom_line() +
  labs(title = 'total, averaged over 5 minute intervals', x = 'date')
total_min
ggsave("figures/total_min.png")

chillin_min <- ggplot(minutes, aes(x = minutes, y = r201_chillin)) +
  geom_line() +
  labs(title = 'chillin, averaged over 5 minute intervals', x = 'date')
chillin_min
ggsave("figures/chillin_min.png")

chillout_min <- ggplot(minutes, aes(x = minutes, y = r201_chillout)) +
  geom_line() +
  labs(title = 'chillout, averaged over 5 minute intervals', x = 'date')
chillout_min
ggsave("figures/chillout_min.png")

coolflow_min <- ggplot(minutes, aes(x = minutes, y = r201_coolflow)) +
  geom_line() +
  labs(title = 'coolflow, averaged over 5 minute intervals', x = 'date')
coolflow_min
ggsave("figures/coolflow_min.png")

smbsflow_min <- ggplot(minutes, aes(x = minutes, y = r201_smbsflow)) +
  geom_line() +
  labs(title = 'smbsflow, averaged over 5 minute intervals', x = 'date')
smbsflow_min
ggsave("figures/smbsflow_min.png")
#####

##### hours
hours <- R201 %>%
  select(-date, -time, -date_time, -minutes, -daily, -weekly) %>%
  group_by(hourly) %>%
  summarise_all(list(mean)) %>%
  data.frame()

msecs_hour <- ggplot(hours, aes(x = hourly, y = msecs)) +
  geom_line() +
  labs(title = 'msecs, averaged over 1 hour intervals', x = 'date')
msecs_hour
ggsave("figures/msecs_hour.png")

temp_hour <- ggplot(hours, aes(x = hourly, y = r201_201temp)) +
  geom_line() +
  labs(title = 'temp, averaged over 1 hour intervals', x = 'date')
temp_hour
ggsave("figures/temp_hour.png")

total_hour <- ggplot(hours, aes(x = hourly, y = r201_201total)) +
  geom_line() +
  labs(title = 'total, averaged over 1 hour intervals', x = 'date')
total_hour
ggsave("figures/total_hour.png")

chillin_hour <- ggplot(hours, aes(x = hourly, y = r201_chillin)) +
  geom_line() +
  labs(title = 'chillin, averaged over 1 hour intervals', x = 'date')
chillin_hour
ggsave("figures/chillin_hour.png")

chillout_hour <- ggplot(hours, aes(x = hourly, y = r201_chillout)) +
  geom_line() +
  labs(title = 'chillout, averaged over 1 hour intervals', x = 'date')
chillout_hour
ggsave("figures/chillout_hour.png")

coolflow_hour <- ggplot(hours, aes(x = hourly, y = r201_coolflow)) +
  geom_line() +
  labs(title = 'coolflow, averaged over 1 hour intervals', x = 'date')
coolflow_hour
ggsave("figures/coolflow_hour.png")

smbsflow_hour <- ggplot(hours, aes(x = hourly, y = r201_smbsflow)) +
  geom_line() +
  labs(title = 'smbsflow, averaged over 1 hour intervals', x = 'date')
smbsflow_hour
ggsave("figures/smbsflow_hour.png")
#####

##### days
days <- R201 %>%
  select(-date, -time, -date_time, -minutes, -hourly, -weekly) %>%
  group_by(daily) %>%
  summarise_all(list(mean))

msecs_day <- ggplot(days, aes(x = daily, y = msecs)) +
  geom_line() +
  labs(title = 'msecs, averaged over 1 day intervals', x = 'date')
msecs_day
ggsave("figures/msecs_day.png")

temp_day <- ggplot(days, aes(x = daily, y = r201_201temp)) +
  geom_line() +
  labs(title = 'temp, averaged over 1 day intervals', x = 'date')
total_day
ggsave("figures/temp_day.png")

total_day <- ggplot(days, aes(x = daily, y = r201_201total)) +
  geom_line() +
  labs(title = 'total, averaged over 1 day intervals', x = 'date')
total_day
ggsave("figures/total_day.png")

chillin_day <- ggplot(days, aes(x = daily, y = r201_chillin)) +
  geom_line() +
  labs(title = 'chillin, averaged over 1 day intervals', x = 'date')
chillin_day
ggsave("figures/chillin_day.png")

chillout_day <- ggplot(days, aes(x = daily, y = r201_chillout)) +
  geom_line() +
  labs(title = 'chillout, averaged over 1 day intervals', x = 'date')
chillout_day
ggsave("figures/chillout_day.png")

coolflow_day <- ggplot(days, aes(x = daily, y = r201_coolflow)) +
  geom_line() +
  labs(title = 'coolflow, averaged over 1 day intervals', x = 'date')
coolflow_day
ggsave("figures/coolflow_day.png")

smbsflow_day <- ggplot(days, aes(x = daily, y = r201_smbsflow)) +
  geom_line() +
  labs(title = 'smbsflow, averaged over 1 day intervals', x = 'date')
smbsflow_day
ggsave("figures/smbsflow_day.png")
#####

##### weeks
weeks <- R201 %>%
  select(-date, -time, -date_time, -minutes, -hourly, -daily) %>%
  group_by(weekly) %>%
  summarise_all(list(mean))

msecs_week <- ggplot(weeks, aes(x = weekly, y = msecs)) +
  geom_line() +
  labs(title = 'msecs, averaged over 1 week intervals', x = 'date')
msecs_week
ggsave("figures/msecs_week.png")

temp_week <- ggplot(weeks, aes(x = weekly, y = r201_201temp)) +
  geom_line() +
  labs(title = 'temp, averaged over 1 week intervals', x = 'date')
temp_week
ggsave("figures/temp_week.png")

total_week <- ggplot(weeks, aes(x = weekly, y = r201_201total)) +
  geom_line() +
  labs(title = 'total, averaged over 1 week intervals', x = 'date')
total_week
ggsave("figures/total_week.png")

chillin_week <- ggplot(weeks, aes(x = weekly, y = r201_chillin)) +
  geom_line() +
  labs(title = 'chillin, averaged over 1 week intervals', x = 'date')
chillin_week
ggsave("figures/chillin_week.png")

chillout_week <- ggplot(weeks, aes(x = weekly, y = r201_chillout)) +
  geom_line() +
  labs(title = 'chillout, averaged over 1 week intervals', x = 'date')
chillout_week
ggsave("figures/chillout_week.png")

coolflow_week <- ggplot(weeks, aes(x = weekly, y = r201_coolflow)) +
  geom_line() +
  labs(title = 'coolflow, averaged over 1 week intervals', x = 'date')
coolflow_week
ggsave("figures/coolflow_week.png")

smbsflow_week <- ggplot(weeks, aes(x = weekly, y = r201_smbsflow)) +
  geom_line() +
  labs(title = 'smbsflow, averaged over 1 week intervals', x = 'date')
smbsflow_week
ggsave("figures/smbsflow_week.png")
#####

grid.arrange(msecs_min, msecs_hour, msecs_day, msecs_week, ncol = 1)
grid.arrange(temp_min, temp_hour, temp_day, temp_week, ncol = 1)
grid.arrange(total_min, total_hour, total_day, total_week, ncol = 1)
grid.arrange(chillin_min, chillin_hour, chillin_day, chillin_week, ncol = 1)
grid.arrange(chillout_min, chillout_hour, chillout_day, chillout_week, ncol = 1)
grid.arrange(coolflow_min, coolflow_hour, coolflow_day, coolflow_week, ncol = 1)
grid.arrange(smbsflow_min, smbsflow_hour, smbsflow_day, smbsflow_week, ncol = 1)
