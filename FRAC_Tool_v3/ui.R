#
# This is the user-interface definition of a Shiny web application. You can
# run the application by clicking 'Run App' above.
#
# Find out more about building applications with Shiny here:
#
#    http://shiny.rstudio.com/
#

library(shiny)

library(magrittr)
library(plyr)
library(dplyr)
library(stats)
library(tidyr)
library(ggplot2)
library(ggthemes)
library(htmlwidgets)
library(plotly)
library(officer)

library(SciViews)
library(drc)
library(nlme)

# library(aomisc)
library(knitr)
library(kableExtra)

library(DT)
library(stringr)

df <- readxl::read_xlsx('FracToolData.xlsx', sheet = '%FR') %>%
  janitor::clean_names()
df$supplier <- ifelse(df$supplier == 'kemira', 'Kemira', df$supplier)

pctFR_v_time <- read.csv('pctFR_v_time.csv')

# Define UI for application that draws a histogram
shinyUI(navbarPage(
  title = "FRAC Tool",
  tabPanel("Description",
           includeMarkdown("overview.Rmd"),
           img(
             src = "Solvay_Logo_POSITIVE_rgb.png",
             height = "15%",
             width = "15%",
             align = "right"
           )),
  tabPanel(
    "%FR v Time",
    h4('Plot % friction reduction versus time for the original (raw) data'),
    fluidPage(
      selectInput(
        "frONE",
        "Friction Reducer(s)",
        choices = c(' ', unique(pctFR_v_time$fr))
      ),
      selectInput("loopONE", "Loop(s)", choices = NULL),
      selectInput("waterONE", "Water(s)", choices = NULL),
      
      # tableOutput("dataONE")
      plotlyOutput("plotONE"),
      
      img(
        src = "Solvay_Logo_POSITIVE_rgb.png",
        height = "15%",
        width = "15%",
        align = "right"
      )
    )
  ),
  tabPanel(
    "Neighbors",
    fluidPage(
      selectInput(
        "frNEIGHBORS",
        "Friction Reducer",
        choices = c(' ', unique(df$fr))
      ),
      selectInput("loopNEIGHBORS", "Loop", choices = NULL),
      selectInput("loadingNEIGHBORS", "Loading (gpt)", choices = NULL),
      selectInput("waterNEIGHBORS", "Water", choices = NULL),
      
    plotlyOutput("neighborsPlot"),
    img(
      src = "BOTH.png",
      height = "25%",
      width = "25%",
      align = "right"
    )
    )
  ),
  tabPanel(
    "Cost Table",
    fluidPage(
      tableOutput('costTable'),
      img(
        src = "O_G-WORDS.png",
        height = "15%",
        width = "15%",
        align = "right"
      )
    )
    ),
  tabPanel(
    "Data",
    fluidPage(
      tableOutput('pctFR_data'),
      img(
        src = "O_G-WORDS.png",
        height = "15%",
        width = "15%",
        align = "right"
      )
    )
  )
))
