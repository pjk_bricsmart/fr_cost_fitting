library(magrittr)
library(plyr)
library(dplyr)
library(stats)
library(tidyr)
library(ggplot2)
library(ggthemes)
library(htmlwidgets)
library(plotly)
library(officer)
library(data.table)

library(SciViews)
library(drc)
library(nlme)

library(aomisc)

df <- readxl::read_xlsx('20200214 FR Cost Comparison Template (Snyder Data).xlsx', sheet = 'All Data')

column_names <- c('Expt', 'FR', 'FR Type', 'LOOP', 'Supplier', 'Loading (gpt)', 'Water',
                  '0.00', '0.17', '0.33', '0.50', '0.67', '0.83', '1.00', '1.17', '1.33',
                  '1.50', '1.67', '1.83', '2.00', '2.17', '2.33', '2.50', '2.67', '2.83',
                  '3.00', '3.17', '3.33', '3.50', '3.67', '3.83', '4.00', '4.17', '4.33',
                  '4.50', '4.67', '4.83', '5.00', 'Average', '3 gpt', '6 gpt', 'Date',
                  'Batch No', 'Sample', 'Pipe ID', 'Flow Rate')

colnames(df) <- column_names

df <- df %>%
  filter(`Loading (gpt)` != 'A' & `Loading (gpt)` != 'k')

dim(unique(df %>% dplyr::select(FR, Water)))

df2 <- df %>% dplyr::select(FR, Water, `Loading (gpt)`, `0.17`) %>%
  tidyr::pivot_wider(names_from = `Loading (gpt)`, values_from = `0.17`)

df2[is.na(df2)] <- 0
df2$`0.1` <- ifelse(df2$`0.1` > 0, TRUE, FALSE)
df2$`0.2` <- ifelse(df2$`0.2` > 0, TRUE, FALSE)
df2$`0.3` <- ifelse(df2$`0.3` > 0, TRUE, FALSE)
df2$`0.5` <- ifelse(df2$`0.5` > 0, TRUE, FALSE)
df2$`1` <- ifelse(df2$`1` > 0, TRUE, FALSE)


