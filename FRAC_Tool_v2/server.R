library(shiny)

# Define server logic required to draw a histogram
shinyServer(function(input, output, session) {
    observeEvent(input$controller, {
        updateTabsetPanel(session, "switcher", selected = input$controller)
    })
})
