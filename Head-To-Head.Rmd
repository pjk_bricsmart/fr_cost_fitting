---
title: "Plexslick 926C vs Chemstream 9800"
author: "Solvay Oil & Gas"
date: "`r Sys.Date()`"
output: slidy_presentation
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
```

```{r}
load("plots.RData")
```

## 0.25 gpt Head to Head

```{r plot_one, fig.align = 'center'}
PLOT_I
```

## %FR vs Loading

```{r plot_two, fig.align = 'center'}
PLOT_II
```

## %FR vs Cost ($/Mgal Frac Fluid)

```{r plot_three, fig.align = 'center'}
PLOT_III
```

## Equivalent Cost of Use 
#### 0.5 gpt Plexslick 926C
```{r plot_four, fig.align = 'center'}
PLOT_IV
```

## Equivalent Cost of Use
#### 0.5 gpt Chemstream 9800
```{r plot_five, fig.align = 'center'}
PLOT_V
```