library(magrittr)
library(plyr)
library(dplyr)
library(stats)
library(tidyr)
library(ggplot2)
library(ggthemes)
library(htmlwidgets)
library(plotly)
library(officer)

library(SciViews)
library(drc)
library(nlme)

library(aomisc)

input <- read.csv('Results_Asymptote_RateConstant_StdDev_Avgs.csv', stringsAsFactors = FALSE)

df <- input %>%
  dplyr::filter(Time == 'Avg1min')

#####

SLIDES <- read_pptx()

#####

for (i in seq(1:nrow(df))) {
  
titleString <- paste0(df[i, 'FR'], ' | ', df[i, 'Water'])

X <- seq(0.0, 1.0, by = 0.01) %>%
  data.frame()
colnames(X) <- c('Loading')
Y <- df[i, 'Asymptote']*(1 - exp((-df[i, 'RateConstant'])*X)) %>%
  data.frame()
colnames(Y) <-  c('pctFR')
data2plot <- cbind(X, Y)

graf <- ggplot2::ggplot(data2plot, aes(x = Loading, y = pctFR)) +
  geom_line() +
  ylim(-1.0, 100) +
  ggtitle(titleString) +
  xlab('Loading (gpt)') +
  ylab('% Friction Reduction') +
  ggthemes::theme_hc()

SLIDES <- add_slide(SLIDES)
SLIDES <- ph_with(x = SLIDES, value = graf, location = ph_location(width = 8, height = 6))

}

#####

print(SLIDES, target = 'pctFR_vs_Loading_Avg1min.pptx')

#####
