library(magrittr)
library(plyr)
library(dplyr)
library(stats)
library(tidyr)
library(ggplot2)
library(ggthemes)
library(htmlwidgets)
library(plotly)
library(officer)
library(data.table)

library(SciViews)
library(drc)
library(nlme)

library(aomisc)

df <- readxl::read_xlsx('20200214 FR Cost Comparison Template (Snyder Data).xlsx', sheet = 'All Data')

column_names <- c('Expt', 'FR', 'FR Type', 'LOOP', 'Supplier', 'Loading (gpt)', 'Water',
                  '0.00', '0.17', '0.33', '0.50', '0.67', '0.83', '1.00', '1.17', '1.33',
                  '1.50', '1.67', '1.83', '2.00', '2.17', '2.33', '2.50', '2.67', '2.83',
                  '3.00', '3.17', '3.33', '3.50', '3.67', '3.83', '4.00', '4.17', '4.33',
                  '4.50', '4.67', '4.83', '5.00', 'Average', '3 gpt', '6 gpt', 'Date',
                  'Batch No', 'Sample', 'Pipe ID', 'Flow Rate')

colnames(df) <- column_names


# PARAMETERS --------------------------------------------------------------

WATER <- '50k'
FR_ONE <- 'FR-HPA'
FR_ONE_COST <- 7.50
FR_TWO <- 'DR-22430'
FR_TWO_COST <- 4.75

FR_TWO_LOADING <- 0.75

FR_TWO_Mgal <- FR_TWO_COST * FR_TWO_LOADING

FR_ONE_Mgal <- FR_TWO_Mgal
FR_ONE_LOADING <- FR_ONE_Mgal / FR_ONE_COST

# ONE ---------------------------------------------------------------------

ONE <- df %>%
  filter(FR == FR_ONE) %>%
  filter(Water == WATER) %>%
  filter(`Loading (gpt)` == 'A' | `Loading (gpt)` == 'k') %>%
  dplyr::select(-Expt, -FR, -`FR Type`, -LOOP, -Supplier, -Water, -Average,
                -`3 gpt`, -`6 gpt`, -Date, -`Batch No`, - Sample, -`Pipe ID`, -`Flow Rate`)

ONE <- as.data.frame(t(ONE))

ONE$Time <- row.names(ONE)

ONE <- ONE[-1, ]

colnames(ONE) <- c('A', 'k', 'Time')

ONE <- as.data.frame(sapply(ONE, as.numeric))

ONE[is.na(ONE)] = 0

ONE$Time <- as.numeric(ONE$Time)
ONE$A <- as.numeric(ONE$A)
ONE$k <- as.numeric(ONE$k)

ONE$A <- ONE$A * 100.0

ONE$pctFR <- ONE$A * (1 - exp(-ONE$k * FR_ONE_LOADING))

ONEplot <- ONE %>%
  dplyr::select(Time, pctFR) %>%
  dplyr::mutate(FR = FR_ONE)

# ggplot2::ggplot(ONE, aes(x = Time, y = pctFR)) + geom_line() + ylim(0.0, 100.0)

# TWO ---------------------------------------------------------------------

TWO <- df %>%
  filter(FR == FR_TWO) %>%
  filter(Water == WATER) %>%
  filter(`Loading (gpt)` == 'A' | `Loading (gpt)` == 'k') %>%
  dplyr::select(-Expt, -FR, -`FR Type`, -LOOP, -Supplier, -Water, -Average,
                -`3 gpt`, -`6 gpt`, -Date, -`Batch No`, - Sample, -`Pipe ID`, -`Flow Rate`)

TWO <- as.data.frame(t(TWO))

TWO$Time <- row.names(TWO)

TWO <- TWO[-1,]

colnames(TWO) <- c('A', 'k', 'Time')

TWO[is.na(TWO)] = 0

TWO$Time <- as.numeric(TWO$Time)
TWO$A <- as.numeric(TWO$A)
TWO$k <- as.numeric(TWO$k)

TWO$A <- TWO$A * 100.0

TWO$pctFR <- TWO$A * (1 - exp(-TWO$k * FR_TWO_LOADING))

TWOplot <- TWO %>%
  dplyr::select(Time, pctFR) %>%
  dplyr::mutate(FR = FR_TWO)

# ggplot2::ggplot(TWO, aes(x = Time, y = pctFR)) + geom_line() + ylim(0.0, 100.0)

# PLOT --------------------------------------------------------------------

data2plot <- rbind(ONEplot, TWOplot)

### Build subtitle:
SUBTITLE <- paste0(FR_ONE_LOADING, ' gpt ', FR_ONE, '-', WATER, '\n', FR_TWO_LOADING, ' gpt ', FR_TWO, '-', WATER)

ggplot2::ggplot(data2plot, aes(x = Time, y = pctFR, colour = FR)) +
  geom_line(size = 0.75) +
  ylim(0.0, 100) +
  labs(title = "Cost = Constant\n% Friction Reduction versus Time (min)",
       # subtitle = "0.48 gpt FR-HPA-50k\n0.76 gpt DR-22430-50k",
       subtitle = SUBTITLE,
       caption = "Solvay Friction Reducer Analysis Comparison Tool\nCONFIDENTIAL") +
  xlab('Time (min)') +
  ylab('% Friction Reduction') +
  ggthemes::theme_stata()
