library(shiny)

library(magrittr)
library(plyr)
library(dplyr)
library(stats)
library(tidyr)
library(ggplot2)
library(ggthemes)
library(htmlwidgets)
library(plotly)
library(officer)

library(SciViews)
library(drc)
library(nlme)

# library(aomisc)
library(knitr)
library(kableExtra)

library(DT)
library(stringr)

# read data ---------------------------------------------------------------

df <- readxl::read_xlsx('FracToolData.xlsx', sheet = '%FR') %>%
    janitor::clean_names()
df$supplier <- ifelse(df$supplier == 'kemira', 'Kemira', df$supplier)

pctFR_v_time <- read.csv('pctFR_v_time.csv')

CostTable <- read.csv('CostTable.csv')

# Define server logic required to draw a histogram
shinyServer(function(input, output, session) {
    
    cost_table <- reactive({
        CostTable
    })
    
    allesONE <- reactive({
        filter(pctFR_v_time, fr == input$frONE)
    })
    
    observeEvent(allesONE(), {
        choices <- unique(allesONE()$loop)
        updateSelectInput(session, "loopONE", choices = c(' ', choices)) 
    })
    
    loopDiameter <- reactive({
        req(input$loopONE)
        filter(allesONE(), loop == input$loopONE)
    })
    
    observeEvent(loopDiameter(), {
        choices <- unique(loopDiameter()$water)
        updateSelectInput(session, "waterONE", choices = c(' ', choices))
    })
    
    data4plotONE <- reactive({
        req(input$waterONE)
        filter(pctFR_v_time, fr == input$frONE & loop == input$loopONE & water == input$waterONE)
    })
    
    
    output$plotONE <- renderPlotly({
        data2plot <- data4plotONE() %>%
            dplyr::select(fr, fr_type, loop, flow_rate_gpm, supplier, loading_gpt,
                water, calcium_content_mg_l, magnesium_content_mg_l, chloride_content_mg_l,
                time, pctFR
            ) %>%
            unique()
        data2plot$pctFR <- data2plot$pctFR * 100.0
        if (nrow(data2plot) == 0) return(NULL)
        p <- ggplot2::ggplot(data = data2plot, aes(
                x = time,
                y = pctFR,
                color = as.factor(loading_gpt)
            )) +
            geom_line(size = 0.75) +
            ylim(-1.0, 100) +
            # ggtitle(buildTitle) +
            xlab('Time (min.)') +
            ylab('% Friction Reduction') +
            ggthemes::theme_hc()
        ggplotly(p)
    })
    
    output$costTable <- renderTable({ cost_table()},  
                              hover = TRUE, spacing = 'xs',  
                              align = 'r')
    
    output$pctFR_data <- renderTable({ df },  
                                    hover = TRUE, spacing = 'xs',  
                                    align = 'r')

    # neighbors ---------------------------------------------------------------
    
    alles <- reactive({
        df
    })
    
    allesNEIGHBORS <- reactive({
        filter(df, fr == input$frNEIGHBORS)
    })
    
    observeEvent(allesNEIGHBORS(), {
        choices <- unique(allesNEIGHBORS()$loop)
        updateSelectInput(session, "loopNEIGHBORS", choices = c(' ', choices)) 
    })
    
    loopDiameterNEIGHBORS <- reactive({
        req(input$loopNEIGHBORS)
        filter(allesNEIGHBORS(), loop == input$loopNEIGHBORS)
    })
    
    observeEvent(loopDiameterNEIGHBORS(), {
        choices <- unique(loopDiameterNEIGHBORS()$loading_gpt)
        updateSelectInput(session, "loadingNEIGHBORS", choices = c(' ', choices))
    })
    
    loadingNEIGHBORS <- reactive({
        req(input$loadingNEIGHBORS)
        filter(loopDiameterNEIGHBORS(), loading_gpt == input$loadingNEIGHBORS)
    })
    
    observeEvent(loadingNEIGHBORS(), {
        choices <- unique(loadingNEIGHBORS()$water)
        updateSelectInput(session, "waterNEIGHBORS", choices = c(' ', choices))
    })
    
    data4plotNEIGHBORS <- reactive({
        req(input$waterNEIGHBORS)
        filter(
            df,
            fr == input$frNEIGHBORS &
                loop == input$loopNEIGHBORS &
                loading_gpt == loadingNEIGHBORS & water == input$waterNEIGHBORS
        )
    })
    
    output$neighborsPlot <- renderPlotly({
        
        euclid <- alles() %>%
            dplyr::select(
                -x5_min_average_30sec,
                -x5_min_average_1_min,
                -x10_min_average_30_sec,
                -x10_min_average_1_min
            )
        
        # needle
        # FR <- 'Kemflow 4251'
        # LOOP <- '3/4"'
        # LOADING <- 0.1
        # WATER <- '50k'
        FR <- input$frNEIGHBORS
        LOOP <- input$loopNEIGHBORS
        LOADING <- input$loadingNEIGHBORS
        WATER <- input$waterNEIGHBORS
        
        needle <- euclid %>%
            filter(fr == FR) %>%
            filter(loop == LOOP) %>%
            filter(loading_gpt == LOADING) %>%
            filter(water == WATER)
        
        x <- needle[ , startsWith(names(needle), 'x')] %>%
            data.frame()
        x <- t(x) %>%
            as.vector()
        
        y <- euclid[ , startsWith(names(euclid), 'x')] 
        for(i in 1:nrow(y)){
            q <- y[i,]
            q <- t(q) %>%
                as.vector()
            DIST <- 0.0
            for(j in 1:length(q)){
                DIST <- DIST + (x[j] - q[j])**2
            }
            DIST <- sqrt(DIST)
            euclid[i, 'distance'] <- DIST
        }
        
        euclid <- euclid %>%
            data.frame()
        
        euclid <- euclid[order(euclid$distance), ]
        
        euclid2 <- euclid %>%
            filter(water == euclid[1, 'water']) %>%
            filter(loop == euclid[1, 'loop'])
        
        n5 <- euclid2[1:6, ]
        
        n5_pivot <- n5 %>%
            gather(-c(fr, fr_type, loop, flow_rate_gpm, supplier, loading_gpt, water,
                      calcium_content_mg_l, magnesium_content_mg_l,
                      chloride_content_mg_l, distance), key = 'time', value = 'pctFR')
            # tidyr::pivot_longer(-c(fr, fr_type, loop, flow_rate_gpm, supplier, loading_gpt, water,
                                   # calcium_content_mg_l, magnesium_content_mg_l,
                                   # chloride_content_mg_l, distance), names_to = 'time', values_to = 'pctFR')
        n5_pivot$time <- str_replace(n5_pivot$time, 'x', '')
        n5_pivot$time <- str_replace(n5_pivot$time, '_', '.')
        n5_pivot$time <- as.numeric(n5_pivot$time)
        
        data4plot <- n5_pivot %>%
            dplyr::select(fr, fr_type, loop, flow_rate_gpm, supplier, loading_gpt,
                          water, calcium_content_mg_l, magnesium_content_mg_l, chloride_content_mg_l,
                          distance, time, pctFR
            ) %>%
            unique()
        data4plot$pctFR <- data4plot$pctFR * 100.0
        if (nrow(data4plot) == 0) return(NULL)
        
        data4plot$id <- paste0(data4plot$fr, '_', data4plot$loading_gpt)
        p <- ggplot2::ggplot(data = data4plot, aes(
            x = time,
            y = pctFR,
            color = as.factor(id)
        )) +
            geom_line(size = 0.75) +
            ylim(-1.0, 100) +
            # ggtitle(buildTitle) +
            xlab('Time (min.)') +
            ylab('% Friction Reduction') +
            ggthemes::theme_hc()
        ggplotly(p)
    })

})

