# install / load packages -------------------------------------------------

if (!require("pacman")) install.packages("pacman")
library(pacman)
pacman::p_load(magrittr,
               plyr,
               dplyr,
               stats,
               tidyr,
               ggplot2,
               ggthemes,
               htmlwidgets,
               plotly,
               officer,
               data.table,
               SciViews,
               drc,
               nlme,
               janitor,
               stringr)

# retrieve data -----------------------------------------------------------

df <- readxl::read_xlsx('FracToolData.xlsx', sheet = 'Ak')

df2 <- df %>%
  dplyr::select(-Title, - Supplier, -`Calcium Content (mg/L)`,
                -`Magnesium Content (mg/L)`, -`Chloride Content (mg/L)`,
                -`5 min Average (30sec)`, -`5 min Average (1 min)`,
                -`10 min Average (30 sec)`, -`10 min Average (1 min)`)

# select products to compare ----------------------------------------------

water <- '150k'
loop <- "1/2\""
loading <- 0.25
prd_one <- 'Plexslick 926C'
prd_one_cost <- 6.75
prd_two <- 'Steamstream 9800'
prd_two_cost <- 10.00
timePt <- 'FiveMinAvg1Min'

# filter dataframe to reflect choices -------------------------------------

df3 <- df2 %>%
  filter(FR == prd_one | FR == prd_two) %>%
  filter(LOOP == loop) %>%
  filter(Water == water)
flow <- unique(df3$`Flow Rate (gpm)`)

# %FR versus Time ---------------------------------------------------------

one <- df3 %>%
  filter(FR == prd_one) %>%
  filter(Water == water) %>%
  dplyr::select(-FR, -`FR Type`, -LOOP, -`Flow Rate (gpm)`, -Water)
one <- as.data.frame(t(one))
one$Time <- row.names(one)
one <- one[-1, ]
colnames(one) <- c('A', 'k', 'Time')
one <- as.data.frame(sapply(one, as.numeric))
one[is.na(one)] = 0
one$A <- one$A * 100.0
one$pctFR <- one$A * (1 - exp(-one$k * loading))
oneplot <- one %>%
  dplyr::select(Time, pctFR) %>%
  dplyr::mutate(FR = prd_one)

two <- df3 %>%
  filter(FR == prd_two) %>%
  filter(Water == water) %>%
  dplyr::select(-FR, -`FR Type`, -LOOP, -`Flow Rate (gpm)`, -Water)
two <- as.data.frame(t(two))
two$Time <- row.names(two)
two <- two[-1, ]
colnames(two) <- c('A', 'k', 'Time')
two <- as.data.frame(sapply(two, as.numeric))
two[is.na(two)] = 0
two$A <- two$A * 100.0
two$pctFR <- two$A * (1 - exp(-two$k * loading))
twoplot <- two %>%
  dplyr::select(Time, pctFR) %>%
  dplyr::mutate(FR = prd_two)

data2plot <- rbind(oneplot, twoplot)

TITLE <- paste0(water, " TDS // ", flow, " gpm // ", loading, " gpt // ", loop, " OD")

PLOT_I <- ggplotly(ggplot2::ggplot(data2plot, aes(x = Time, y = pctFR, colour = FR)) +
           geom_line(size = 0.75) +
           ylim(0.0, 100) +
           labs(title = TITLE) +
           xlab('Time (min)') +
           ylab('% Friction Reduction') +
           ggthemes::theme_hc()
         )

# %FR versus Loading ------------------------------------------------------

df_loading <- df %>%
  dplyr::select(-Title, - Supplier, -`Calcium Content (mg/L)`,
                -`Magnesium Content (mg/L)`, -`Chloride Content (mg/L)`) %>%
  filter(FR == prd_one | FR == prd_two) %>%
  filter(LOOP == loop) %>%
  filter(Water == water) %>%
  dplyr::select(FR, `Loading (gpt)`, '0.5', '1', '2', '5', `5 min Average (30sec)`,
         `5 min Average (1 min)`, `10 min Average (30 sec)`, `10 min Average (1 min)`)
newColumnNames <- c('FR', 'Ak', 'ThirtySec', 'OneMin', 'TwoMin', 'FiveMin',
                    'FiveMinAvg30Sec', 'FiveMinAvg1Min', 'TenMinAvg30Sec', 'TenMinAvg1Min')
colnames(df_loading) <- newColumnNames

X <- seq(0.0, 1.0, by = 0.01) %>%
    data.frame()
colnames(X) <- c('Loading')

one <- df_loading %>%
  dplyr::filter(FR == prd_one)
A <- one %>% dplyr::filter(Ak == 'A') %>% .[[timePt]]
A <- A * 100.0
k <- one %>% dplyr::filter(Ak == 'k') %>% .[[timePt]]
  
Y1 <- A * (1 - exp((-k) * X)) %>%
  data.frame()
colnames(Y1) <- c(prd_one)
  
one_plot <- cbind(X, Y1) %>%
  mutate(FR = prd_one) %>%
  rename('pctFR' = prd_one)
  
two <- df_loading %>%
  dplyr::filter(FR == prd_two)
A <- two %>% dplyr::filter(Ak == 'A') %>% .[[timePt]]
A <- A * 100.0
k <- two %>% dplyr::filter(Ak == 'k') %>% .[[timePt]]

Y2 <- A * (1 - exp((-k) * X)) %>%
  data.frame()
colnames(Y2) <- c(prd_two)

two_plot <- cbind(X, Y2) %>%
  mutate(FR = prd_two) %>%
  rename('pctFR' = prd_two)
  
data2plot <- rbind(one_plot, two_plot)

TITLE2 <- paste0(TITLE, "\n %FR vs Loading\t\t\t\t\t[", timePt, "]")

PLOT_II <- ggplotly(
  ggplot2::ggplot(data2plot, aes(
    x = Loading, y = pctFR, colour = FR
  )) +
    geom_line(size = 0.75) +
    ylim(-1.0, 100) +
    labs(title = TITLE2) +
    xlab('Loading (gpt)') +
    ylab('% Friction Performance') +
    ggthemes::theme_hc()
)

# %FR versus cost ---------------------------------------------------------

data2plot$cost <-
  ifelse(
    data2plot$FR == prd_one,
    data2plot$Loading * prd_one_cost,
    data2plot$Loading * prd_two_cost
  )

TITLE3 <- paste0(TITLE, "\n", "%FR vs Cost ($/Mgal Frac Fluid)\t\t\t\t\t[", timePt, "]")

PLOT_III <- ggplotly(
  ggplot2::ggplot(data2plot, aes(
    x = cost, y = pctFR, colour = FR
  )) +
    geom_line(size = 0.75) +
    ylim(-1.0, 100) +
    labs(title = TITLE3) +
    xlab('Cost ($/Mgal)') +
    ylab('% Friction Reduction') +
    ggthemes::theme_hc()
)

# equivalent cost of use / prd_one fixed ----------------------------------

water <- '150k'
loop <- "1/2\""
# loading <- 0.25
prd_one <- 'Plexslick 926C'
prd_one_cost <- 6.75
prd_two <- 'Steamstream 9800'
prd_two_cost <- 10.00
timePt <- 'FiveMinAvg1Min'

prd_one_loading <- signif(0.5, 2)
prd_one_Mgal <- signif(prd_one_cost * prd_one_loading, 2)
prd_two_Mgal <- signif(prd_one_Mgal, 2)
prd_two_loading <- signif(prd_two_Mgal / prd_two_cost, 2)
equivalent_cost <- signif(prd_one_loading * prd_one_cost, 3)

one <- df3 %>%
  filter(FR == prd_one) %>%
  filter(Water == water) %>%
  dplyr::select(-FR, -`FR Type`, -LOOP, -`Flow Rate (gpm)`, -Water)
one <- as.data.frame(t(one))
one$Time <- row.names(one)
one <- one[-1, ]
colnames(one) <- c('A', 'k', 'Time')
one <- as.data.frame(sapply(one, as.numeric))
one[is.na(one)] = 0
one$A <- one$A * 100.0
one$pctFR <- one$A * (1 - exp(-one$k * prd_one_loading))
oneplot <- one %>%
  dplyr::select(Time, pctFR) %>%
  dplyr::mutate(FR = prd_one)

two <- df3 %>%
  filter(FR == prd_two) %>%
  filter(Water == water) %>%
  dplyr::select(-FR, -`FR Type`, -LOOP, -`Flow Rate (gpm)`, -Water)
two <- as.data.frame(t(two))
two$Time <- row.names(two)
two <- two[-1, ]
colnames(two) <- c('A', 'k', 'Time')
two <- as.data.frame(sapply(two, as.numeric))
two[is.na(two)] = 0
two$A <- two$A * 100.0
two$pctFR <- two$A * (1 - exp(-two$k * prd_two_loading))
twoplot <- two %>%
  dplyr::select(Time, pctFR) %>%
  dplyr::mutate(FR = prd_two)

data2plot <- rbind(oneplot, twoplot)

TITLE <- paste0(water, " TDS // ", flow, " gpm // ", loop, " OD")

annotation_one <-
  paste0(prd_one_loading,
         " gpt ",
         prd_one,
         " - ",
         water,
         " ($",
         equivalent_cost,
         "/Mgal)")
annotation_two <-
  paste0(prd_two_loading,
         " gpt ",
         prd_two,
         " - ",
         water,
         " ($",
         equivalent_cost,
         "/Mgal)")

PLOT_IV <- ggplotly(ggplot2::ggplot(data2plot, aes(x = Time, y = pctFR, colour = FR)) +
           geom_line(size = 0.75) +
           ylim(0.0, 100) +
           labs(title = TITLE) +
           xlab('Time (min)') +
           ylab('% Friction Reduction') +
           annotate("text", x = 2.5, y = 95, label = annotation_one) +
           annotate("text", x = 2.5, y = 90, label = annotation_two) +
           ggthemes::theme_hc()
)

# equivalent cost of use / prd_two fixed ----------------------------------

water <- '150k'
loop <- "1/2\""
# loading <- 0.25
prd_one <- 'Plexslick 926C'
prd_one_cost <- 6.75
prd_two <- 'Steamstream 9800'
prd_two_cost <- 10.00
timePt <- 'FiveMinAvg1Min'

prd_two_loading <- signif(0.5, 2)
prd_two_Mgal <- signif(prd_two_cost * prd_two_loading, 2)
prd_one_Mgal <- signif(prd_two_Mgal, 2)
prd_one_loading <- signif(prd_one_Mgal / prd_one_cost, 2)
equivalent_cost <- signif(prd_two_loading * prd_two_cost, 3)

one <- df3 %>%
  filter(FR == prd_one) %>%
  filter(Water == water) %>%
  dplyr::select(-FR, -`FR Type`, -LOOP, -`Flow Rate (gpm)`, -Water)
one <- as.data.frame(t(one))
one$Time <- row.names(one)
one <- one[-1, ]
colnames(one) <- c('A', 'k', 'Time')
one <- as.data.frame(sapply(one, as.numeric))
one[is.na(one)] = 0
one$A <- one$A * 100.0
one$pctFR <- one$A * (1 - exp(-one$k * prd_one_loading))
oneplot <- one %>%
  dplyr::select(Time, pctFR) %>%
  dplyr::mutate(FR = prd_one)

two <- df3 %>%
  filter(FR == prd_two) %>%
  filter(Water == water) %>%
  dplyr::select(-FR, -`FR Type`, -LOOP, -`Flow Rate (gpm)`, -Water)
two <- as.data.frame(t(two))
two$Time <- row.names(two)
two <- two[-1, ]
colnames(two) <- c('A', 'k', 'Time')
two <- as.data.frame(sapply(two, as.numeric))
two[is.na(two)] = 0
two$A <- two$A * 100.0
two$pctFR <- two$A * (1 - exp(-two$k * prd_two_loading))
twoplot <- two %>%
  dplyr::select(Time, pctFR) %>%
  dplyr::mutate(FR = prd_two)

data2plot <- rbind(oneplot, twoplot)

TITLE <- paste0(water, " TDS // ", flow, " gpm // ", loop, " OD")

annotation_one <-
  paste0(prd_one_loading,
         " gpt ",
         prd_one,
         " - ",
         water,
         " ($",
         equivalent_cost,
         "/Mgal)")
annotation_two <-
  paste0(prd_two_loading,
         " gpt ",
         prd_two,
         " - ",
         water,
         " ($",
         equivalent_cost,
         "/Mgal)")

PLOT_V <- ggplotly(ggplot2::ggplot(data2plot, aes(x = Time, y = pctFR, colour = FR)) +
           geom_line(size = 0.75) +
           ylim(0.0, 100) +
           labs(title = TITLE) +
           xlab('Time (min)') +
           ylab('% Friction Reduction') +
           annotate("text", x = 2.5, y = 95, label = annotation_one) +
           annotate("text", x = 2.5, y = 90, label = annotation_two) +
           ggthemes::theme_hc()
)

# save plots --------------------------------------------------------------

save(PLOT_I, PLOT_II, PLOT_III, PLOT_IV, PLOT_V, file = "plots.RData")
