library(magrittr)
library(plyr)
library(dplyr)
library(stats)
library(tidyr)
library(ggplot2)
library(ggthemes)
library(htmlwidgets)
library(plotly)
library(officer)
library(data.table)

library(SciViews)
library(drc)
library(nlme)

library(aomisc)

df <- readxl::read_xlsx('20200214 FR Cost Comparison Template (Snyder Data).xlsx', sheet = 'All Data')

column_names <- c('Expt', 'FR', 'FR Type', 'LOOP', 'Supplier', 'Loading (gpt)', 'Water',
                  '0.00', '0.17', '0.33', '0.50', '0.67', '0.83', '1.00', '1.17', '1.33',
                  '1.50', '1.67', '1.83', '2.00', '2.17', '2.33', '2.50', '2.67', '2.83',
                  '3.00', '3.17', '3.33', '3.50', '3.67', '3.83', '4.00', '4.17', '4.33',
                  '4.50', '4.67', '4.83', '5.00', 'Average', '3 gpt', '6 gpt', 'Date',
                  'Batch No', 'Sample', 'Pipe ID', 'Flow Rate')

colnames(df) <- column_names


# PARAMETERS --------------------------------------------------------------

WATER <- '50k'
FR_ONE <- 'FR-HPA'
FR_ONE_COST <- 7.50
FR_TWO <- 'DR-22430'
FR_TWO_COST <- 4.75

FR_TWO_LOADING <- 0.75

FR_TWO_Mgal <- FR_TWO_COST * FR_TWO_LOADING

FR_ONE_Mgal <- FR_TWO_Mgal
# FR_ONE_LOADING <- FR_ONE_Mgal / FR_ONE_COST
FR_ONE_LOADING <- FR_TWO_LOADING

# TWENTY ---------------------------------------------------------------------

TWENTY <- df %>%
  filter(FR == FR_ONE) %>%
  filter(Water == WATER) %>%
  filter(`Loading (gpt)` == 'A' | `Loading (gpt)` == 'k') %>%
  dplyr::select(-Expt, -FR, -`FR Type`, -LOOP, -Supplier, -Water, -Average,
                -`3 gpt`, -`6 gpt`, -Date, -`Batch No`, - Sample, -`Pipe ID`, -`Flow Rate`)

TWENTY <- as.data.frame(t(TWENTY))

TWENTY$Time <- row.names(TWENTY)

TWENTY <- TWENTY[-1, ]

colnames(TWENTY) <- c('A', 'k', 'Time')

TWENTY <- as.data.frame(sapply(TWENTY, as.numeric))

TWENTY[is.na(TWENTY)] = 0

TWENTY$Time <- as.numeric(TWENTY$Time)
TWENTY$A <- as.numeric(TWENTY$A)
TWENTY$k <- as.numeric(TWENTY$k)

TWENTY$A <- TWENTY$A * 100.0

TWENTY$pctFR <- TWENTY$A * (1 - exp(-TWENTY$k * 0.20))

TWENTYplot <- TWENTY %>%
  dplyr::select(Time, pctFR) %>%
  dplyr::mutate(FR = FR_ONE) %>%
  dplyr::mutate(Loading = '0.20')

# FORTY ---------------------------------------------------------------------

FORTY <- df %>%
  filter(FR == FR_ONE) %>%
  filter(Water == WATER) %>%
  filter(`Loading (gpt)` == 'A' | `Loading (gpt)` == 'k') %>%
  dplyr::select(-Expt, -FR, -`FR Type`, -LOOP, -Supplier, -Water, -Average,
                -`3 gpt`, -`6 gpt`, -Date, -`Batch No`, - Sample, -`Pipe ID`, -`Flow Rate`)

FORTY <- as.data.frame(t(FORTY))

FORTY$Time <- row.names(FORTY)

FORTY <- FORTY[-1, ]

colnames(FORTY) <- c('A', 'k', 'Time')

FORTY <- as.data.frame(sapply(FORTY, as.numeric))

FORTY[is.na(FORTY)] = 0

FORTY$Time <- as.numeric(FORTY$Time)
FORTY$A <- as.numeric(FORTY$A)
FORTY$k <- as.numeric(FORTY$k)

FORTY$A <- FORTY$A * 100.0

FORTY$pctFR <- FORTY$A * (1 - exp(-FORTY$k * 0.40))

FORTYplot <- FORTY %>%
  dplyr::select(Time, pctFR) %>%
  dplyr::mutate(FR = FR_ONE) %>%
  dplyr::mutate(Loading = '0.40')

# SIXTY -------------------------------------------------------------------

SIXTY <- df %>%
  filter(FR == FR_ONE) %>%
  filter(Water == WATER) %>%
  filter(`Loading (gpt)` == 'A' | `Loading (gpt)` == 'k') %>%
  dplyr::select(-Expt, -FR, -`FR Type`, -LOOP, -Supplier, -Water, -Average,
                -`3 gpt`, -`6 gpt`, -Date, -`Batch No`, - Sample, -`Pipe ID`, -`Flow Rate`)

SIXTY <- as.data.frame(t(SIXTY))

SIXTY$Time <- row.names(SIXTY)

SIXTY <- SIXTY[-1, ]

colnames(SIXTY) <- c('A', 'k', 'Time')

SIXTY <- as.data.frame(sapply(SIXTY, as.numeric))

SIXTY[is.na(SIXTY)] = 0

SIXTY$Time <- as.numeric(SIXTY$Time)
SIXTY$A <- as.numeric(SIXTY$A)
SIXTY$k <- as.numeric(SIXTY$k)

SIXTY$A <- SIXTY$A * 100.0

SIXTY$pctFR <- SIXTY$A * (1 - exp(-SIXTY$k * 0.60))

SIXTYplot <- SIXTY %>%
  dplyr::select(Time, pctFR) %>%
  dplyr::mutate(FR = FR_ONE) %>%
  dplyr::mutate(Loading = '0.60')

# EIGHTY ------------------------------------------------------------------

EIGHTY <- df %>%
  filter(FR == FR_ONE) %>%
  filter(Water == WATER) %>%
  filter(`Loading (gpt)` == 'A' | `Loading (gpt)` == 'k') %>%
  dplyr::select(-Expt, -FR, -`FR Type`, -LOOP, -Supplier, -Water, -Average,
                -`3 gpt`, -`6 gpt`, -Date, -`Batch No`, - Sample, -`Pipe ID`, -`Flow Rate`)

EIGHTY <- as.data.frame(t(EIGHTY))

EIGHTY$Time <- row.names(EIGHTY)

EIGHTY <- EIGHTY[-1, ]

colnames(EIGHTY) <- c('A', 'k', 'Time')

EIGHTY <- as.data.frame(sapply(EIGHTY, as.numeric))

EIGHTY[is.na(EIGHTY)] = 0

EIGHTY$Time <- as.numeric(EIGHTY$Time)
EIGHTY$A <- as.numeric(EIGHTY$A)
EIGHTY$k <- as.numeric(EIGHTY$k)

EIGHTY$A <- EIGHTY$A * 100.0

EIGHTY$pctFR <- EIGHTY$A * (1 - exp(-EIGHTY$k * 0.80))

EIGHTYplot <- EIGHTY %>%
  dplyr::select(Time, pctFR) %>%
  dplyr::mutate(FR = FR_ONE) %>%
  dplyr::mutate(Loading = '0.80')

# PLOT --------------------------------------------------------------------

data2plot <- rbind(TWENTYplot, FORTYplot, SIXTYplot, EIGHTYplot)

### Build subtitle:
SUBTITLE <- paste0(FR_ONE_LOADING, ' gpt ', FR_ONE, '-', WATER, '\n', FR_TWO_LOADING, ' gpt ', FR_TWO, '-', WATER)

ggplot2::ggplot(data2plot, aes(x = Time, y = pctFR, colour = Loading)) +
  geom_line(size = 0.75) +
  ylim(0.0, 100) +
  labs(title = "Friction Reduction versus Time (min)",
       # subtitle = "0.48 gpt FR-HPA-50k\n0.76 gpt DR-22430-50k",
       # subtitle = SUBTITLE,
       caption = "Solvay Friction Reducer Analysis Comparison Tool\nCONFIDENTIAL") +
  xlab('Time (min)') +
  ylab('% Friction Reduction') +
  ggthemes::theme_stata()
