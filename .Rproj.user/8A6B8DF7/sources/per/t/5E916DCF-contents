# install / load packages -------------------------------------------------

if (!require("pacman")) install.packages("pacman")
library(pacman)
pacman::p_load(magrittr,
               plyr,
               dplyr,
               stats,
               tidyr,
               ggplot2,
               ggthemes,
               htmlwidgets,
               plotly,
               officer,
               data.table,
               SciViews,
               drc,
               nlme,
               janitor,
               stringr)

# retrieve data -----------------------------------------------------------

df <- readxl::read_xlsx('FracToolData.xlsx', sheet = 'Ak')

df2 <- df %>%
  dplyr::select(FR, LOOP, "Flow Rate (gpm)", Water, "Loading (gpt)", "0.5", "1", "2", "5", "10",
                "5 min Average (30sec)", "5 min Average (1 min)",
                "10 min Average (30 sec)", "10 min Average (1 min)")

# curate dataframe --------------------------------------------------------

colnames <- c("FR", "LOOP", "FLOW_RATE", "WATER", "LOADING", "ThirtySec", "OneMin", "TwoMin",
              "FiveMin", "TenMin", "FiveMin30Avg", "FiveMin60Avg", "TenMin30Avg",
              "TenMin60Avg")
colnames(df2) <- colnames

# pivot dataframe ---------------------------------------------------------

df3 <-
  tidyr::gather(
    df2,
    key = "TIME",
    value = "measurement",
    -c("FR", "LOOP", "FLOW_RATE", "WATER", "LOADING")
  )

df4 <- tidyr::spread(df3, LOADING, measurement)

# plot(s) -----------------------------------------------------------------

df4$LABEL <- paste0(df4$FR, ' ', df4$LOOP, ' ', df4$FLOW_RATE, ' ', df4$WATER, ' ', df4$`TIME`)

PLOT_A <- ggplotly(
  ggplot2::ggplot(df4, aes(x = k, y = A, color = LOOP, label = LABEL)) + geom_point() + theme_tufte()
)
PLOT_A

PLOT_B <- ggplotly(
  ggplot2::ggplot(df4, aes(x = k, y = A, color = as.factor(FLOW_RATE), label = LABEL)) + geom_point() + theme_tufte()
)
PLOT_B

PLOT_C <- ggplotly(
  ggplot2::ggplot(df4, aes(x = k, y = A, color = WATER, label = LABEL)) + geom_point() + theme_tufte()
)
PLOT_C

PLOT_D <- ggplotly(
  ggplot2::ggplot(df4, aes(x = k, y = A, color = `TIME`, label = LABEL)) + geom_point() + theme_tufte()
)
PLOT_D

PLOT_E <- ggplotly(
  ggplot2::ggplot(df4, aes(x = k, y = A, color = FR, label = LABEL)) + geom_point() + theme_tufte()
)
PLOT_E

save(PLOT_A, PLOT_B, PLOT_C, PLOT_D, PLOT_E, file = "Avk_plots.RData")

qaz <- df4 %>%
  filter(FR == 'DR-42430') # %>%
  # filter(`TIME` == 'TwoMin')
ggplotly(
  ggplot2::ggplot(qaz, aes(x = k, y = A, color = LOOP, label = LABEL)) + geom_point() + theme_tufte()
)

qaz <- df4 %>%
  filter(FR == 'Plexslick 930') # %>%
  # filter(`TIME` == 'TwoMin')
ggplotly(
  ggplot2::ggplot(qaz, aes(x = k, y = A, color = LOOP, label = LABEL)) + geom_point() + theme_tufte()
)
