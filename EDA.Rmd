---
title: "Friction Reducers"
subtitle: "Snyder Data"
description: |
  Exploratory Data Analysis (EDA) of the Snyder Data
author:
  - name: Dru BISHOP
    affiliation: US Commercial Director / NA-Chemplex Sales
  - name: Eddie BUNGE
    affiliation: Global Marketing Director / Oil & Gas Market
  - name: John POKORNY
    affiliation: Business Development Manager / Oil & Gas - New Business Development
  - name: Henry WIGGINS
    affiliation: Technical Service Director / NA-Technical Sales
  - name: Paul J Kowalczyk 
    affiliation: Senior Data Scientist / Corporate R & I
date: "`r Sys.Date()`"
output: 
  distill::distill_article:
    toc: true
    toc_depth: 2
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)

library(magrittr)
library(plyr)
library(dplyr)
library(stats)
library(tidyr)
library(ggplot2)
library(ggthemes)
library(htmlwidgets)
library(plotly)
library(officer)
library(data.table)

# df <- readxl::read_xlsx('20200214 FR Cost Comparison Template (Snyder Data).xlsx', sheet = 'All Data')

```

# Data  







