library(magrittr)
library(plyr)
library(dplyr)
library(stats)
library(tidyr)
library(ggplot2)
library(ggthemes)
library(htmlwidgets)
library(plotly)
library(officer)
library(data.table)

library(SciViews)
library(drc)
library(nlme)

library(aomisc)


# install / load packages -------------------------------------------------

if (!require("pacman")) install.packages("pacman")
library(pacman)
pacman::p_load(magrittr,
               plyr,
               dplyr,
               stats,
               tidyr,
               ggplot2,
               ggthemes,
               htmlwidgets,
               plotly,
               officer,
               data.table,
               SciViews,
               drc,
               nlme,
               janitor,
               stringr)

# ingest data -------------------------------------------------------------

df <- readxl::read_xlsx('FracToolData.xlsx', sheet = '%FR') %>%
  janitor::clean_names()

df$supplier <- ifelse(df$supplier == 'kemira', 'Kemira', df$supplier)

# pivot -------------------------------------------------------------------

df_pivot <- df %>%
  dplyr::select(-x5_min_average_30sec, -x5_min_average_1_min,
                -x10_min_average_30_sec, -x10_min_average_1_min) %>%
  tidyr::pivot_longer(-c(fr, fr_type, loop, flow_rate_gpm, supplier, loading_gpt, water,
                         calcium_content_mg_l, magnesium_content_mg_l,
                         chloride_content_mg_l), names_to = 'time', values_to = 'pctFR')
df_pivot$time <- str_replace(df_pivot$time, 'x', '')
df_pivot$time <- str_replace(df_pivot$time, '_', '.')
df_pivot$time <- as.numeric(df_pivot$time)

write.csv(df_pivot, file = 'pctFR_v_time.csv', row.names = FALSE)

# EDA: A & k --------------------------------------------------------------

ak_unique <- df_ak %>%
  dplyr::select(fr, fr_type, loop, flow_rate_gpm, supplier, water, calcium_content_mg_l,
                magnesium_content_mg_l, chloride_content_mg_l) %>%
  unique()

water <- df_ak %>%
  dplyr::select(water, loop, calcium_content_mg_l,
                magnesium_content_mg_l, chloride_content_mg_l) %>%
  unique()

qaz <- df_ak %>%
  dplyr::select(fr,
                supplier,
                fr_type,
                loop,
                water,
                flow_rate_gpm) %>%
  unique()

qaz2 <- df_ak %>%
  dplyr::select(fr,
                loop,
                water) %>%
  unique()

# Euclidean Distance ------------------------------------------------------

euclid <- df %>%
  dplyr::select(
    -x5_min_average_30sec,
    -x5_min_average_1_min,
    -x10_min_average_30_sec,
    -x10_min_average_1_min
  )

# needle
FR <- 'Kemflow 4251'
LOOP <- '3/4"'
LOADING <- 0.1
WATER <- '50k'
  
needle <- euclid %>%
  filter(fr == FR) %>%
  filter(loop == LOOP) %>%
  filter(loading_gpt == LOADING) %>%
  filter(water == WATER)

x <- needle[ , startsWith(names(needle), 'x')] %>%
  data.frame()
x <- t(x) %>%
  as.vector()

y <- euclid[ , startsWith(names(euclid), 'x')] 
for(i in 1:nrow(y)){
  q <- y[i,]
  q <- t(q) %>%
    as.vector()
  DIST <- 0.0
  for(j in 1:length(q)){
    DIST <- DIST + (x[j] - q[j])**2
  }
  DIST <- sqrt(DIST)
  euclid[i, 'distance'] <- DIST
}

euclid <- euclid %>%
  data.frame()

euclid <- euclid[order(euclid$distance), ]

euclid2 <- euclid %>%
  filter(water == euclid[1, 'water']) %>%
  filter(loop == euclid[1, 'loop'])

n5 <- euclid2[1:6, ]

n5_pivot <- n5 %>%
  tidyr::pivot_longer(-c(fr, fr_type, loop, flow_rate_gpm, supplier, loading_gpt, water,
                         calcium_content_mg_l, magnesium_content_mg_l,
                         chloride_content_mg_l, distance), names_to = 'time', values_to = 'pctFR')
n5_pivot$time <- str_replace(n5_pivot$time, 'x', '')
n5_pivot$time <- str_replace(n5_pivot$time, '_', '.')
n5_pivot$time <- as.numeric(n5_pivot$time)

data2plot <- n5_pivot %>%
  dplyr::select(fr, fr_type, loop, flow_rate_gpm, supplier, loading_gpt,
                water, calcium_content_mg_l, magnesium_content_mg_l, chloride_content_mg_l,
                distance, time, pctFR
  ) %>%
  unique()
data2plot$pctFR <- data2plot$pctFR * 100.0
if (nrow(data2plot) == 0) return(NULL)

data2plot$id <- paste0(data2plot$fr, '_', data2plot$loading_gpt)
p <- ggplot2::ggplot(data = data2plot, aes(
  x = time,
  y = pctFR,
  color = as.factor(id)
)) +
  geom_line(size = 0.75) +
  ylim(-1.0, 100) +
  # ggtitle(buildTitle) +
  xlab('Time (min.)') +
  ylab('% Friction Reduction') +
  ggthemes::theme_hc()
ggplotly(p)
